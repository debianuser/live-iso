#!/bin/bash
#
# set -x
#
# 2023-11-14
echo -e "---- isomaster setup ----\n"
CONFIG_FILE=config.isomaster.sh
echo -e "     config: $CONFIG_FILE"
echo -e "real config: $(dirname "$(readlink -f "$CONFIG_FILE")")/$(basename "$(readlink -f "$CONFIG_FILE")")"
source $CONFIG_FILE
# show all variables from config
# ( set -o posix ; set ) | grep "X_"
# exit
# show script name
# https://www.baeldung.com/linux/find-bash-script-filename
echo -e '     script:' $(basename "$0")
echo -e "real script: $(dirname "$(readlink -f "$0")")/$(basename "$(readlink -f "$0")")"
echo -e "\n---- end of setup ----"
# exit
## >>>>> live-iso >>>>>>>>>>>>>>>
# essentials
MAIN_RESOURCE_DIR=/Resources4LiveIso
#
WORK_DIR=/live-iso
[ ! -d "$WORK_DIR" ] && echo "$WORK_DIR is missing - check!" && exit
#DEBIAN_ISO="$MAIN_RESOURCE_DIR"/Debian12-ISO/12_1/debian-live-12.1.0-amd64-lxde.iso
DEBIAN_ISO="$MAIN_RESOURCE_DIR"/"$X_SUBDIR_ISO"/"$X_FILE_ISO"
[ ! -f "$DEBIAN_ISO" ] && echo "$DEBIAN_ISO is missing  - check!" && exit
# latest kernel version has to be linked via 'ln -s' on softlink "latest"
### 2023-09-20
KERNEL_PACKAGE_DIR="$MAIN_RESOURCE_DIR"/Kernels/latest
[ ! -d "$KERNEL_PACKAGE_DIR" ] && echo "$KERNEL_PACKAGE_DIR is missing  - check!" && exit
### 2023-09-20
# associative arrays #
# main and sole working array
declare -A var
# helping to find something
declare -a dirList
declare -a pathList
declare -a namesList
# lists of function patterns which help filtering functions for display 
declare -a ctbankixFunctionList
declare -a functions2ExcludeFrom_List2
#
# >>>>>>>>>>>> Funktionen >>>>>>>>>>>>>>>><
#
# >>>>>>>>>> Dialog >>>>>>>>>>
#
__getDialogOptions () {
    #echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #
    local -n myArrayRef="$1"

    myArrayRef=(\
	    listAllFunctionNames \
        listUserFunctions \
        printVarsSorted \
		printVars \
        listDirectoryKeys \
		listPathKeys \
        viewResources \
        viewResourcesSimple \
        view_WorkDirectory \
        createInfrastructure \
        iso_resources_mutability_check \
        getChrootEnv \
	    chrootMountCheck \
    )
    
    if [[ $EUID -eq 0 ]]
    then
        echo -e "function added for root)""\n"
        myArrayRef+=(handleChrootAspects)
        myArrayRef+=(handleMutabilityOfResources)  # a submenu
        #myArrayRef+=(fs_in_work-getRidOfElderKernels)
        myArrayRef=(menu4Remaster chrootRescueFunction fs_in_work-getRidOfElderKernels - isoCreate runNewIso "${myArrayRef[@]}")

    else

        local baseDir
        baseDir="$(getVar BASE_DIR)"
        if [ ! -d "$baseDir" ]; then
            myArrayRef=(createInfrastructure)
            myArrayRef+=(view_WorkDirectory)
        else         
            myArrayRef+=(showEUID)
            myArrayRef+=(becomeSudo)
            myArrayRef+=(runNewIso)
        fi
 
        # # https://stackoverflow.com/questions/54773652/is-there-a-way-to-prepend-to-a-bash-array-without-writing-a-function
        # #myArrayRef=(becomeSudo "${myArrayRef[@]}")
        # myArrayRef+=(showEUID)
        # myArrayRef+=(becomeSudo)
    fi


}
#
__checkDirectoryExists () {
    [[ -d $1 ]] # e.g. test file exists
}

__baseDirIsMissing () {
    local ownerOfWorkDir=$(stat -c '%U' $WORK_DIR)
    local ownerString="Become user $(echoBoldWhiteOnMagenta $ownerOfWorkDir) (who owns $WORK_DIR) and execute"

#    [[ $USER = $ownerOfWorkDir ]] && echo "user is owner" || echo "user is not owner"
    [[ $USER = $ownerOfWorkDir ]] && ownerString=""
    clear
    cat << EOF
$(getVar BASE_DIR) is missing.
It has to be created.

$ownerString

$(echoBoldWhiteOnMagenta "bash $(basename $0) createInfrastructure")

to create $(getVar BASE_DIR) and it's substructure.

EOF
}
#
check__BaseDirExists () {
    local baseDir="$(getVar BASE_DIR)"
    __checkDirectoryExists $baseDir && echo "$baseDir exist" || __baseDirIsMissing  && exit 
}
#
__getIndexOfElementInArray () {
    #echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #declare -p index
    local -n myArrayRef="$1"
    local -n element="$2"
    local -n wanted="$3"
    for i in "${!myArrayRef[@]}";
    do
        #echo $i
        if [[ "${myArrayRef[$i]}" = "${element}" ]];
        then
            #echo "${myArrayRef[$i]}"
            wanted=$i
            break
        fi
    done
}
# __menuImpl () {
#     local -n options="$1"
#     local superFunction="$2"
#     #echo "superFunction : ${superFunction}"
#     echo -e "Funktion: $(echoBoldWhiteOnMagenta  $superFunction)\n"
#     local end="quit"
#     options+=("$end")
#     local index=-1
#     __getIndexOfElementInArray options end index
#     ((index+=1))
#     local PS3="select function ( use ^C or  $index to leave, <enter> to re-list options of $superFunction} ) : "
#     local opt
#     local end="quit"
#     COLUMNS=1
#     select opt in "${options[@]}" ; do
# 	    if [[ "$opt" == "$end" ]]
# 	    then 
# 	        echo -e "\n" && echoBoldWhiteOnGreen "$superFunction " && echo -e " . . . finished""\n" && return
#     	else 
# 	        $"$opt"
# 	    fi
#     done
# }

#
__menuImplGoodBye () {
    trap "" RETURN
    echo -e "\n" && echoBoldWhiteOnGreen "$1" && echo -e " . . . finished""\n"  
}
#
__menuImpl () {
    trap "__menuImplGoodBye $2" RETURN
    local -n options="$1"
    local superFunction="$2"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  $superFunction)\n"
    local end="return"  
    options+=("$end")
    local index=-1
    __getIndexOfElementInArray options end index
    ((index+=1))
    local PS3="select function ( use ^C or  $index to leave, <enter> to re-list options of $superFunction} ) : "
    COLUMNS=1
    select opt in "${options[@]}" ; do
        $"$opt"
    done
}
#
menu4Remaster () {
    local myOptions=(\
        makeClean \
        createInfrastructure \
        unpackIsoAndCreate_fs_in_work \
        fs_in_work-SetupPersonalVersion \
        - \
        fs_in_work-upgradePackages
        fs_in_work-upgradeKernel \
        - \
        fs_in_work-MakeNewSquashfs \
        replaceFilesystemSquashfs \
        - \
        isoPack \
        -
        #runNewIso \
    )
    echo "calling __menuImpl"
    __menuImpl myOptions ${FUNCNAME[0]}
}
#
# one space behind an option enforces an empty line
menu4ChrootAspects () {
    local myOptions=(\
        chrootRescueFunction \
        getChrootEnv \
        chrootMountCheck \
        chrootPrepareAndMount \
        chrootRestoreAndUmount \
    )
    __menuImpl myOptions ${FUNCNAME[0]}
}
- () {
    ---
}
# help to organise menu for readability
--- () {
    echo ""
}
#
menu4MutabilityAspects () {
    local myOptions=(\
        iso_resources_mutability_check \
        --- 
        iso_resources_change_2_immutable \
        iso_resources_reset_2_mutable \
        ---
        )
    __menuImpl myOptions ${FUNCNAME[0]}
}
#
handleChrootAspects () {
    ## submenu 2
    echo -e "\n\n"
    #local PS3='Please enter sub option: '
    local options=(chrootRestoreAndUmount chrootMountCheck getChrootEnv chrootPrepareAndMount)
    #
    local end="quit"
    local options+=("$end")
    local index=-1
    __getIndexOfElementInArray options end index
    ((index+=1))

    local PS3="select function ( use ^C or  $index to leave, <enter> to list options of function ${FUNCNAME[0]} ) : "
    #"Sub menu item 1" "Sub menu item 2" "quit")
    local opt
    local end="quit"

#  select opt in "${options[@]}"
#   do
#       case $opt in
#           "Sub menu item 1")
#               echo "you chose sub item 1"
#               ;;
#           "Sub menu item 2")
#               echo "you chose sub item 2"
#               ;;
#           "Sub menu quit")
#               return
#               ;;
#           *) echo "invalid option $REPLY";;
#       esac
#   done

    select opt in "${options[@]}" ; do
	#echo "$opt"
	if [[ "$opt" == "$end" ]]
	then 
	    echo -e "leave function handleMutabilityOfResources\n\n"; return
    # solution : just enter <enter> :-)
    # elif [[ "$opt" == "again" ]]
    # then 
    #     continue
	else 
	    $"$opt"
	fi
    done

}
#
handleMutabilityOfResources () {
    ## submenu 1
    echo -e "\n\n"
    #local PS3='Please enter sub option: '
    local options=(iso_resources_mutability_check iso_resources_change_2_immutable  iso_resources_reset_2_mutable)
    #
    local end="quit"
    local options+=("$end")
    local index=-1
    __getIndexOfElementInArray options end index
    ((index+=1))

    local PS3="select function ( use ^C or  $index to leave, <enter> to list options of function ${FUNCNAME[0]} ) : "
    #"Sub menu item 1" "Sub menu item 2" "quit")
    local opt
    local end="quit"

#  select opt in "${options[@]}"
#   do
#       case $opt in
#           "Sub menu item 1")
#               echo "you chose sub item 1"
#               ;;
#           "Sub menu item 2")
#               echo "you chose sub item 2"
#               ;;
#           "Sub menu quit")
#               return
#               ;;
#           *) echo "invalid option $REPLY";;
#       esac
#   done

    select opt in "${options[@]}" ; do
	#echo "$opt"
	if [[ "$opt" == "$end" ]]
	then 
	    echo -e "leave function handleMutabilityOfResources\n\n"; return
    # solution : just enter <enter> :-)
    # elif [[ "$opt" == "again" ]]
    # then 
    #     continue
	else 
	    $"$opt"
	fi
    done

}

#
__dialog () {
    echo -e "\n showing: $(echoBoldWhiteOnMagenta  options) for user : $(echoBoldWhiteOnMagenta $USER)\n"
    declare -a options
    __getDialogOptions options
    local end="exit"
    local options+=("$end")
    local index=-1
    __getIndexOfElementInArray options end index
    ((index+=1))
    local title="available functions ( $USER )"
    local prompt="select function ( use ^C or  $index to leave, <enter> to re-list options ) : "
    echo -e "$title\n"
    PS3="$prompt"
    COLUMNS=1
    #COLUMNS=0
    select opt in "${options[@]}" ; do
	#echo "$opt"
	if [[ "$opt" == "$end" ]]
	then 
	    echo "Goodbye"; break
    # solution : just enter <enter> :-)
    # elif [[ "$opt" == "again" ]]
    # then 
    #     continue
	else 
        echo -e "\n"
	    $"$opt"
	fi
    done
}
# <<<<<<<<<< Dialog <<<<<<<<<<
#
# >>>>>>>>>> Funktionen aus ctbankix >>>>>>>>>>
# copied from https://github.com/ctbankix-continuation-team/ctbankix-continuation/blob/master/ctbankix_lubuntu_20.04.3.sh
# the script that inspired the idea of making this developer tool :-=
function resetText()				{ echo -n "$(tput sgr0)"; }
function setTextBold()				{ echo -n "$(tput bold)"; }
function setTextWhite()				{ echo -n "$(tput setaf 7)"; }
function setBackgroundRed()			{ echo -n "$(tput setab 1)"; }
function setBackgroundGreen()		{ echo -n "$(tput setab 2)"; }
function setBackgroundYellow()		{ echo -n "$(tput setab 3)"; }
function setBackgroundBlue()		{ echo -n "$(tput setab 4)"; }
function setBackgroundMagenta()		{ echo -n "$(tput setab 5)"; }
function setBackgroundCyan()		{ echo -n "$(tput setab 6)"; }

function echoBoldWhiteOnRed ()		{ setTextBold; setTextWhite; setBackgroundRed;		echo -n "$1"; resetText; }
function echoBoldWhiteOnGreen ()	{ setTextBold; setTextWhite; setBackgroundGreen;	echo -n "$1"; resetText; }
function echoBoldWhiteOnYellow ()	{ setTextBold; setTextWhite; setBackgroundYellow;	echo -n "$1"; resetText; }
function echoBoldWhiteOnBlue ()		{ setTextBold; setTextWhite; setBackgroundBlue;		echo -n "$1"; resetText; }
function echoBoldWhiteOnMagenta ()	{ setTextBold; setTextWhite; setBackgroundMagenta;	echo -n "$1"; resetText; }
function echoBoldWhiteOnCyan ()		{ setTextBold; setTextWhite; setBackgroundCyan;		echo -n "$1"; resetText; }

function echoDateTime()				{ echoBoldWhiteOnYellow "[$(date '+%x %X')]"; }

function __CheckExitCodePreviousCommand {
	if [ "$?" -eq 0 ]
	then
		echoBoldWhiteOnGreen "[PASS]"; echo;
	else
		echoBoldWhiteOnRed "[FAIL]"; echo;
	fi
}
# >>>>> Ende ctbankix-continuation >>>>>>>>
# used to index functions
ctbankixFunctionList=( "resetText" \
    "setText" \
    "setBackground" \
    "echoBoldWhite" \
    "__CheckExitCodePreviousCommand"
    )
functions2ExcludeFrom_List2=( "setVar" \
    "setVariables" \
    "getVar" \
    "backup" \
    "retrieveVersionFrom" \
    "__"

    )
#
## >>>>>>>>>>>>>>> Variable setzen >>>>>>>>>>>>>
#
# make it easy to retrieve a value from the big array
getVar () {
    if [ -z "$1" ]                           # Is parameter #1 zero length?
    then
	    echo "${FUNCNAME[0]}: Parameter #1 is zero length."  # Or no parameter passed.
        return
    fi
    echo "${var[$1]}"
}
#
# variables are devided in different categories 
#
# directories
setVariables_Directory () {
    setVar_Dir HOME_DIR "$(pwd)"
    setVar_Dir DEBIAN_ISO_DIR "$(dirname $DEBIAN_ISO)"
    setVar_Dir BASE_DIR "$WORK_DIR"/"${var['DEBIAN_LIVE_VERSION']}"
    setVar_Dir BASE_DIR_MBR ${var['BASE_DIR']}/MBR
    setVar_Dir BACKUP_DIR ${var['BASE_DIR']}/Backup
    setVar_Dir ISO_BASE_DIR ${var['BASE_DIR']}/iso
    setVar_Dir ISO_FIRMWARE_DIR ${var['ISO_BASE_DIR']}/firmware
    setVar_Dir UEFI_GRUB_DIR "${var[ISO_BASE_DIR]}"/boot/grub
    #
    setVar_Dir RESOURCES_SPLASH_DIR "$MAIN_RESOURCE_DIR/Splashes"
    setVar_Dir ISOLINUX_SPLASH_VERSIONPAINTER_DIR /home/wilhelm/WORK/VersionPainter
    setVar_Dir ISO_SPLASH_RESOURCE_DIR ${var['RESOURCES_SPLASH_DIR']}/Isolinux
    setVar_Dir PLYMOUTH_SPLASH_DIR ${var['RESOURCES_SPLASH_DIR']}/Plymouth
    #
    # werden erzeugt vom Auspacken
    setVar_Dir ISO_LIVE_DIR ${var['ISO_BASE_DIR']}/live  
    setVar_Dir ISO_ISOLINUX_DIR ${var['ISO_BASE_DIR']}/isolinux
    setVar_Dir ISO_BASE_MOUNT_DIR ${var["BASE_DIR"]}/ISO_MOUNT_POINT
    setVar_Dir KERNEL_PACKAGE_DIR "$KERNEL_PACKAGE_DIR"
    setVar_Dir FS_IN_WORK_DIR ${var['BASE_DIR']}/fs_in_work
    setVar_Dir PLYMOUTH_SPLASH_TARGET_DIR ${var['FS_IN_WORK_DIR']}/usr/share/plymouth/themes/homeworld
    setVar_Dir TARGET_DIR_4_NEW_ISO ${var['BASE_DIR']}/Targets
    setVar_Dir ETC_SKEL_TARGET_DIR /etc/skel
}
#
# paths
setVariables_Path () {
    setVar_Path DEBIAN_ISO "$DEBIAN_ISO"
    setVar_Path UEFI_GRUB_CFG "${var[UEFI_GRUB_DIR]}"/grub.cfg
    setVar_Path ISOLINUX_CFG "${var[ISO_ISOLINUX_DIR]}"/isolinux.cfg
    setVar_Path ISOLINUX_PNG_640x480 "${var[ISO_ISOLINUX_DIR]}"/splash.png
    setVar_Path ISOLINUX_PNG_800x600 "${var[ISO_ISOLINUX_DIR]}"/splash800x600.png
    setVar_Path ISOLINUX_SPLASH_VERSIONPAINTER "${var['ISOLINUX_SPLASH_VERSIONPAINTER_DIR']}"/versionPainter.102
    #
    setVar_Path ISOLINUX_SPLASH_BASE_PNG  "${var[ISO_SPLASH_RESOURCE_DIR]}"/trez_goarem.splash.png
    setVar_Path PLYMOUTH_SPLASH_RESOURCE_PNG  "${var[PLYMOUTH_SPLASH_DIR]}"/pini_in_der_nacht.png
    setVar_Path PLYMOUTH_SPLASH_TARGET_PNG  "${var[PLYMOUTH_SPLASH_TARGET_DIR]}"/logo.png
    #
    setVar_Path ISOLINUX_SPLASH_TARGET_PATH "${var['ISO_ISOLINUX_DIR']}"/splash.png
    setVar_Path ETC_APT_PREF_KERNEL_PATH /etc/apt/preferences.d/kernel.pref
    setVar_Path ETC_APT_SOURCES_LIST_PATH /etc/apt/sources.list
    setVar_Path ETC_CONNMAN_CONF_PATH /etc/connman/main.conf
    setVar_Path ETC_LOCALE_PATH /etc/locale.gen
    setVar_Path ETC_DEFAULT_LOCALE_PATH /etc/default/locale
    setVar_Path ETC_SKEL_BASHRC_PATH "${var[ETC_SKEL_TARGET_DIR]}"/.bashrc
    setVar_Path ETC_DEFAULT_KEYBOARD_PATH /etc/default/keyboard
    setVar_Path ETC_WPA_SUPPLICANT_PATH /etc/wpa_supplicant/boese_tante.conf
    setVar_Path MBR_PATH $(getVar BASE_DIR_MBR)/"$(getVar MBR_TEMPLATE)"
}
#
# common variables
setVariables_Names () {
    setVar_Name SQUASHFS_FILE filesystem.squashfs
    setVar_Name SQUASHFS_NEW_FILE fs_in_work.squashfs
    setVar_Name MBR_TEMPLATE isohdpfx.bin
}
#
# these are special and only used the tshark context 
setVariables_Tshark () {
    setVar_Path TSHARK_BASH_SCRIPT_PATH /usr/local/bin/tshark-capture.sh
    setVar_Path TSHARK_SYSTEMD_SERVICE_CONFIG_PATH /lib/systemd/system/tshark-capture.service
    setVar_Path TSHARK_SYSTEMD_SYSTEM_MULTI_USER_TARGET_PATH /etc/systemd/system/multi-user.target.wants/tshark-capture.service 
}
#
# the setting of variables build one upon the other
# so ORDER IS IMPORTANT
setVariables_All (){
    # order is important:
    setVariables_Names
    # get versions first , these determine directory names afterwards
    retrieveVersionFromLinuxKernelVanillaPackages
    retrieveVersionFromDebianLiveIso
    # set directories and paths now
    setVariables_Directory
    setVariables_Path
    setVariables_Tshark
}
#
#
retrieveVersionFromLinuxKernelVanillaPackages () {
    # something goes wrong? => activate debug by debug=0
    #echo -e "Funktion \n${FUNCNAME[0]} aufgerufen\n"
    local kernelPackageDir="$KERNEL_PACKAGE_DIR"
    echo -e "$kernelPackageDir"
    local debug=1
    if [[ -L "$kernelPackageDir" && -d "$kernelPackageDir" ]]
       then
            local kernelPackageRealDir=$(realpath $kernelPackageDir)
            if [ "$debug" == 0 ]
            then
                echo -e "\n""$kernelPackageDir is a symlink to a directory\n"
                echo -e "realer Pfad: $kernelPackageRealDir""\n"
        fi
    else
        echo -e "Funktion \n${FUNCNAME[0]} aufgerufen\n"
        echo -e "Something wrong with $search_dir\n"
        echo -e "Tip: activate debug-variable in function\n"
        exit
    fi
    #
    local kernelVersionString=${kernelPackageRealDir##*/}
    echo -e "filename: $kernelVersionString"
    # 2023-11-14
    local kernelVersion=${kernelVersionString##*-}
    #local kernelVersion ${kernelVersionString/linux.//}
    #local kernelVersion="${kernelVersionString//-/.}"
    echo -e "     Kernel is version $kernelVersion"
    setVar_Name KERNEL_VERSION "$kernelVersion"
}
#
retrieveVersionFromDebianLiveIso () {
    #echo -e "Funktion \n${FUNCNAME[0]} aufgerufen\n"
    filename="$DEBIAN_ISO"
    dirname=$(dirname "$filename")
    echo "DIR: $dirname"
    local iso_basename="$(basename -- $filename)"
    echo "FILE: $iso_basename"
    local filename_part_at_end="-amd64-lxde.iso"
    local filename_part_at_start="debian-live"
    local dash="-"
    local number_of_dashes=$(echo "$iso_basename" | tr -cd $dash | wc -c)
    #echo "number_of_dashes = $number_of_dashes"
    #
    #
    if [[ $filename == *$filename_part_at_end* ]] && [[ $filename == *$filename_part_at_start* ]] ; then
	echo "It's there!"
	partX1=${filename%%"-amd64-lxde.iso"}
	echo "partX1: " $partX1
	partX2=${partX1#*"$dirname/debian-live-"}
	echo "partX2: " $partX2
	#echo "$partX2"
	echo -e "Debian live is version $partX2"
	setVar_Name DEBIAN_LIVE_VERSION "$partX2"

     elif 
	 [[  $number_of_dashes == 7  ]] ; then
	 partX1=${iso_basename%%"$dash"*}
	 echo "meine Version: $partX1"
	 setVar_Name DEBIAN_LIVE_VERSION "$partX1"

    else
	setVar_Name DEBIAN_LIVE_VERSION "not_versioned"
    fi
    #
    [[ ! -z $(getVar DEBIAN_LIVE_VERSION) ]] && echo "Not empty" || echo "is empty" 
    #[[ ! -z "$DEBIAN_LIVE_VERSION" ]] && setVar_Name DEBIAN_LIVE_VERSION "unversioned"
    echo -e $(getVar DEBIAN_LIVE_VERSION)
}
# deepest level 
__view_Directory_Impl () {
    [ ! -d "$1" ] && echo -e "\n$(echoBoldWhiteOnBlue $1)  $(echoBoldWhiteOnRed 'is missing - check!'))""\n\n" && return
    #echo -e "Arguments: $#"
    #echo -e "$1"
    # echo -e "treeDepth: $2""\n"
    # return
    echo -e "$(tree -u -a -A -C -L $2 $1)""\n"

}
#
# view "WORK_DIR"
view_WorkDirectory () {
 __view_Directory_Impl "$WORK_DIR" 3  
}
#
# view directories below "MAIN_RESOURCE_DIR" 
__view_MainResourceDirectories () {
    #echo -e "$MAIN_RESOURCE_DIR/$1"
    __view_Directory_Impl "$MAIN_RESOURCE_DIR/$1" "$2"
}
#
viewResourcesSimple () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local treeDepth=2
    __view_MainResourceDirectories "" $treeDepth
}
#
viewResources () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local -a infraStructureList=( "Debian12-ISO" "Kernels" "SKELETON" "Splashes")
    local treeDepth=20
    for dir in ${infraStructureList[@]} 
    do
        echo -e "expected: $dir"
    done

    echo -e "\n"
    for dir in ${infraStructureList[@]} 
    do
        #echo $dir
        #checkInfrastructureImpl $dir
        __view_MainResourceDirectories $dir $treeDepth
    done
    return 
    # checkInfrastructureImpl "BASE_DIR"
    # checkInfrastructureImpl "RESOURCE_BASE_DIR"
}
# step 1 of the whole process
createInfrastructure () {
    #check4User ${FUNCNAME[0]}
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    # owner:group of WORK_DIR
    echo -e "$WORK_DIR""\n"
    local ownerIdOfWorkDir=$(stat -c '%u' $WORK_DIR)
    local groupIdOfWorkDir=$(stat -c '%g' $WORK_DIR)    
    echo -e "ownerId: $ownerIdOfWorkDir \ngroupId: $groupIdOfWorkDir\n"
    # dirs to create
    # stop if BASE_DIR exists
    local baseDir="$(getVar BASE_DIR)"
    if [ -d "$baseDir" ]; then
        # Take action if $DIR exists. #
        echo -e "$baseDir exists - $(echoBoldWhiteOnRed 'clean as root before creating infrastructure a new ...')""\n"
        exit
    fi
    local -a dirs=( 
        "$(getVar BASE_DIR)" \
        "$(getVar BASE_DIR_MBR)"\
        "$(getVar BASE_DIR)/Backup" \
        "$(getVar ISO_BASE_MOUNT_DIR)"\
        "$(getVar BASE_DIR)/Targets"
    )

    for d in "${dirs[@]}"
    do
        echo "creating >> $d <<"
        mkdir -p "$d"
    done
    echo -e "\n\n"
    createMbrTemplate
    baseDirTargets=$(getVar BASE_DIR)/Targets
    workDirTargets=$WORK_DIR/Targets
    echo -e "baseDirTargets: $baseDirTargets"
    echo -e "workDirTargets: $workDirTargets"
    [ -d $workDirTargets ] && echo "removing old $workDirTargets " && rm -rf $workDirTargets
    $(ln -s $baseDirTargets $workDirTargets)
    ##.##chown -R $ownerIdOfWorkDir:$groupIdOfWorkDir $WORK_DIR
}
#
## >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><

__backupImpl () {
#backupFile ()
    if [ -z "$1" ]                           # Is parameter #1 zero length?
    then
	    echo "${FUNCNAME[0]}: Parameter #1 is zero length."  # Or no parameter passed.
        return
    fi
    # 2023-09-21
    if [ ! -f "$1" ]
    then
	echo "File $1 not found!"
	return
    fi
    #
    # Die Aufgabe dieser Funktion ist die Sicherung von Dateien
    # in das Backup-Verzeichnis
    # der Pfad der Datei wird umgewandelt (/ => _)
    # der Dateiname angehängt und die Datei gespeichert
    # wenn die Datei noch nicht vorhanden ist => return 3
    file4Backup=$1
    filename="$(basename $file4Backup)"
    filenamePath="$(dirname $file4Backup)"
    filenameSubstitut="${filenamePath////__}"
    echo "$filenamePath"
    echo " filenamePathSubstitut: $filenameSubstitut"
    #echo "$filename"
    delimiter1="___"
    fileInBackupName="$filenameSubstitut$delimiter1$filename"
    echo "komplett mit Dateiname: $fileInBackupName"
    echo -e "\n"
    backupDir=$(getVar BACKUP_DIR)
    backupPath="$backupDir/$fileInBackupName"
    #echo "backupPathV: "
    echo $backupPath
    if [ -f "$backupPath" ]
    then
        echo -e "\n"
        echoBoldWhiteOnRed "$backupPath is already present" && echo -e "\n"
        echoBoldWhiteOnRed "consider to remove that file manually and repeat your action" && echo -e "\n"
        return
    else 
        echo "Datei noch nicht vorhanden, kopieren"
    fi
    #echo "$backupDir"
    #echo -e "\nZielname: " "$fileInBackupName" "\n"
    #return
    #cp $file4Backup $backupDir/"$fileInBackupName" && (echo -e "\n" && echoBoldWhiteOnGreen "$file4Backup successfully backed up" && echo -e "\n\n")
    cp $file4Backup $backupPath && (echo -e "\n" && echoBoldWhiteOnGreen "$file4Backup successfully backed up" && echo -e "\n\n")
}

# backupFilesFromISOUnpack () {
#     # needs argument!
#     # calls __backupImpl
#     file=$1
#     echo "$file"
#     #local res=
#     __backupImpl "$file"
#     #local res="$?"
#     #echo -e "in sub: $res"
#     #echo $res
# }

__contains() {
    # $1 a list
    # $2 an item
    local -n list="$1"
    local x="$2"
    #[[ $list =~ (^|[[:space:]])$x($|[[:space:]]) ]] && exit(0) || exit(1)
    # for a in ${list[@]}
    # do
    #     echo "a: $a"
    # done
    [[ $list =~ (^|[[:space:]])$x($|[[:space:]]) ]] && echo 'yes' || echo 'no'
}

__checkContains () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local -a list=( eins zwei )
    local item=eins
    result=$(__contains $list $item)
    echo -e "Ergebnis: $?"
    echo -e "Result: $result"
}

__sanityCheckImpl () {
    # echo -e "1: $1""\n"
    # echo -e "2: $2""\n"

    # local -a validArgumentOne
    # validArgumentOne=("Verzeichnis" "Pfad")
    # for argument in ${validArgumentOne[@]}
    # do
    #     echo $argument
    # done 
    #contains $1 ${validArgumentOne[@]}
    # then
    #     echo "valid argument $1"
    # else 
    #     echo 
    #         "$1 not a valid argument"
    # fi
    # return
    #echo "Anzahl Parameter: $#"
    #for parameter; do echo "$parameter"; done
    #echo '$1': "$1"
    #echo '$2': "$2"
    # IFS=' ' read -ra ARRAY <<< "$2"
    # unset IFS;
    #
    # new!
    # make use of a nameref 
    # https://stackoverflow.com/questions/10582763/how-to-return-an-array-in-bash-without-using-globals
    local -n ARRAY="$2"
    #echo -e "\n"
    echo "Anzahl Array-Elemente : ${#ARRAY[@]}"
    set -o pipefail -e

    for element in "${ARRAY[@]}"; do
    	echo "    Key:  $element"
	    echo -e "   Wert:  ${var[$element]}"
	    if [[ "$1" == "Verzeichnis" ]]
	    then
	        #echo -e "Parameter ist Verzeichnis\n"
	        [ -d "${var[$element]}" ]
	    elif [[ "$1" == "Pfad" ]]
	    then
	        #echo -e "Parameter ist Pfad\n"
	        [ -f "${var[$element]}" ] 
	    else
	        echo -e "Funktion ${FUNCNAME[0]} fehlerhaft aufgerufen\n"
	        echo "Weder ... noch ... !!!"
	        break
	    fi
	    echo -e "     ok:  ${var[$element]}\n"
    done
    set +o pipefail +e

}
#
checkDirSanity () {
    # make use of a nameref 
    # https://stackoverflow.com/questions/10582763/how-to-return-an-array-in-bash-without-using-globals
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "ok: Verzeichnis ist vorhanden.\nDie Funktion bricht ohne ok ab, wenn ein Verzeichnis nicht vorhanden ist.\nDer letzte angezeigte Wert ist fehlerhaft.\n"
    __sanityCheckImpl "Verzeichnis" dirList
}
#
checkPathSanity () {
    # make use of a nameref 
    # https://stackoverflow.com/questions/10582763/how-to-return-an-array-in-bash-without-using-globals

    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "$(echoBoldWhiteOnMagenta ok): if path is present.\n\n$(echoBoldWhiteOnRed 'Function aborts without ok'): path is missing!\nThe value shown last is erroneous.\n"
    __sanityCheckImpl "Pfad" pathList
}
#
checkSanity () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    checkDirSanity
    checkPathSanity
    #echo -e "ok: Verzeichnis / Pfad ist vorhanden.\nDie Funktion bricht ohne ok ab, wenn ein Verzeichnis / ein Pfad nicht vorhanden ist.\nDer letzte angezeigte Wert ist fehlerhaft.\n"

    # test: __sanityCheckImpl "names" ctbankixFunctionList
    #__sanityCheckImpl "Pfad" "$pathList"
}
#
listDirectoryKeys () {
    printf 'dirList: %s \n' "${dirList[@]}"
}
listPathKeys () {
        printf 'pathList: %s \n' "${pathList[@]}"
}
listNamesList () {
    printf 'namesList: %s \n' "${namesList[@]}"
}
#
setVar_Dir () {
    var[$1]=$2
    dirList+=($1)
    # Debugging
    #printf "setVar_Dir: %s: %s\n" $1 "${var[$1]}"
    #printf 'dirList: %s \n' "${dirList[@]}"
}
#
setVar_Path () {
    var[$1]=$2
    pathList+=($1)
    # Debugging
    #printf "setFilenameVar: %s: %s\n" $1 "${var[$1]}"
    #printf 'pathList: %s \n' "${pathList[@]}"
}
#
setVar_Name () {
    var[$1]=$2
    dist=" "
    namesList+=($1)
    #$1
    #printf "setVar: %s: %s\n" $1 "${var[$1]}"
    #printf 'namesList: %s \n' "${namesList[@]}"
}

# printDirList () {

#     echo $dirList
#     echo "Anzahl: ${#dirList[@]}"

#     IFS=' ' read -ra ARRAY <<< "$dirList"
#     unset IFS;


#     for k in "${!ARRAY[@]}"
#     do
#         echo $k ' - ' ${ARRAY["$k"]}
#     done |
#     sort -rn -k3
#     return



#     # #$ array=("a c" b f "3 5")
#     # IFS=$'\n' sorted=($(sort <<<"${dirList[*]}")); unset IFS
#     # printf "[%s]\n" "${sorted[@]}"





#     # #
#     # return
#     IFS=' ' read -ra ARRAY <<< "$dirList"
#     unset IFS;
#     #
#     #echo -e "\n"
#     echo "Anzahl Array-Elemente : ${#ARRAY[@]}"
#     #return
#     IFS=$'\n' sorted=($(sort <<<"${ARRAY[*]}")); unset IFS
#     for variable in "${!sorted[@]}"
#     do printf "  %s\n       %s\n" "$variable" "${ARRAY[$variable]}"
#     done
#     echo -e "\n"
# }
#
# >>>>>>>>>>>>>>>>>>>>>>>>> iso live <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#
isoLiveDir_DeleteFilesButSquashfs () {
    local xPWD=$(pwd) 
    cd $(getVar "ISO_LIVE_DIR")
    #echo -e "PWD: $(pwd)""\n"
    ## check this: -> find . -type f -not -name "filesystem.squashfs" -delete
    find . -type f -not -name "$(getVar SQUASHFS_FILE)" -delete
    #
    cd $xPWD
}
#
isoLiveDir_UpgradeBootFiles () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #
    # a new kernel needs a new filesystem.squashfs within it's live directory
    # so: all files iso/live/* can be removed in one step
    #
    local kernelVersion=$(getVar "KERNEL_VERSION")
    echo "kernel version: $kernelVersion"
    echo -e "\n"
    #
    local fs_in_work_Dir=$(getVar "FS_IN_WORK_DIR")
    #echo "$fs_in_work_Dir"
    local fs_in_work_BootDir=$fs_in_work_Dir/boot
    echo "from $fs_in_work_BootDir"
    # Ziel 
    local liveDir=$(getVar "ISO_LIVE_DIR")
    #echo "target dir: $liveDir"
    echo -e "copy kernel- and initrd-files to $liveDir""\n"
    #
    # clear old files but filesystem.squashfs
    $(isoLiveDir_DeleteFilesButSquashfs)
    #
    for entry in "$fs_in_work_BootDir"/*"$kernelVersion"*
    do
        echo "file to copy: $entry"
        cp "$entry" "$liveDir"/ 
    done
}
#  
getText4IsolinuxCfg () {
    local kernelVersion="$(getVar KERNEL_VERSION)"
    #echo "$kernelVersion" 
    local myDate=$(printf "%(%Y-%m-%d %H:%M)T")
    #echo "$myDate"
    echo -e "$(cat <<SETVAR
# wjp 2022-03-10 <$myDate>
# D-I config version 2.0
# search path for the c32 support libraries (libcom32, libutil etc.)
path 
DEFAULT German (de)
LABEL German (de)
  SAY "live-iso (de) build on debian $(getVar DEBIAN_LIVE_VERSION) ($myDate) with vanilla kernel $kernelVersion"
  linux /live/vmlinuz-$kernelVersion
  APPEND initrd=/live/initrd.img-$kernelVersion boot=live components locales=de_DE.UTF-8 username=bankix timezone=Europe/Berlin quiet splash
prompt 0
timeout 0
SETVAR
)"
}
#
changeISOBootIsolinuxCfg () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo "erzeugt isolinux/isolinux.cfg"
    echo -e "\n"
    local isolinuxCfgText=$(getText4IsolinuxCfg)
    #echo"$isolinuxCfgText"
    local isolinuxCfgFile=$(getVar ISOLINUX_CFG)
    echo -e "target file: $isolinuxCfgFile""\n"
    echo -e "$isolinuxCfgText" > "$isolinuxCfgFile"
}
#
changeISOBoot () {
    isoLiveDir_UpgradeBootFiles
    #isoLiveDirUpgradeBootFiles
    changeISOBootIsolinuxCfg
}
#
getPath4FileInBackupDir () {
    # echo "$1"
    # return
    local backupDir=$(getVar BACKUP_DIR)
    local pattern="*$1"
    #echo -e "$backupDir""\n"
    local backupFile="$backupDir"/"$pattern"
    #echo -e "gesucht: $backupFile""\n"
    echo "$(ls $backupFile)"
    #echo -e "$path""\n"
}
#
changeUefiBootGrubCfg (){
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo "erzeuge grub.cfg für uefi-Boot"
    #
    header="## iso-live home made"
    #grub.cfg
    vorlage="$(getPath4FileInBackupDir grub.cfg)"
    echo -e "Vorlage: $vorlage""\n"
    newFile=$(getVar "UEFI_GRUB_CFG")
    echo -e "  Ziel:  $newFile""\n"
    #
    printf "%s %(%Y-%m-%d %H:%M)T\n" "$header" > "$newFile"
    #
    # start by copying lines from the old configuration
    #
    while IFS= read -r line
    do
        # erspare viele Header-Zeilen am Anfang des Scripts
        [[ $line =~ ^"$header".* ]] && continue
        [[ $line =~ ^menuentry.* ]] && break
        printf '%s\n' "$line" >> $newFile
    done <"$vorlage"
    # set values for booting
    # set default="0"
    # set timeout=0
    printf "\n## default 0 calls first menuentry,\n## timeout 0 => instantly!\n"  >> "$newFile"
    printf '%s\n' "set default=\"0\"" >> $newFile
    printf '%s\n\n' "set timeout=0" >> $newFile
    # menuentry konstruieren
    title=$(printf 'menuentry \"live-iso - build date: %(%Y-%m-%d %H:%M)T\" {')
#    echo "Kernel-Version" "${var[KERNEL_VERSION]}"
    vmlinuzVAR=$(printf "%b" "  linux  /live/vmlinuz-${var[KERNEL_VERSION]} boot=live components locales=de_DE.UTF-8 username=bankix timezone=Europe/Berlin quiet splash")
#    echo "**$vmlinuzVAR**"
    initrdVAR="  initrd /live/initrd.img-${var[KERNEL_VERSION]}"
#    echo $initrdVAR
    uefiBootEntry=$(printf "%s\n%b\n%b\n%s" "$title" "$vmlinuzVAR" "$initrdVAR" "}")
    printf '%b\n' "$uefiBootEntry" >> "$newFile"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
}

getChrootEnv () {
    # fs_in_work is where we are going with the chroot command
    echo "$(getVar FS_IN_WORK_DIR)"
}
# first of a pair of important functions
chrootPrepareAndMount () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    ## prepare files
    # 1.) DNS
    cp /etc/resolv.conf "$CHROOT_ENV"/etc/resolv.conf
    # 2.) sources.list
    #/usr/bin/cp $(realpath /etc/apt/sources.list) "$CHROOT_ENV"/etc/apt/sources.list
    fs_in_work_chrooted-setup_sourceslist
    ## done
    # now mount
    echoBoldWhiteOnGreen "mounting dev" && echo -e "\n"
    mount --bind /dev "$CHROOT_ENV"/dev
    echoBoldWhiteOnGreen "mounting dev pts" && echo -e "\n"
    mount -t devpts devpts "$CHROOT_ENV"/dev/pts
    echoBoldWhiteOnGreen "mounting proc" && echo -e "\n"
    mount -t proc proc "$CHROOT_ENV"/proc
    echoBoldWhiteOnGreen "mounting sys" && echo -e "\n"
    mount -t sysfs sysfs "$CHROOT_ENV"/sys
}
#
# second of a pair of important functions
# call this function if the script error-exited leaving the chroot environment mounted
# here is a second name to simplify restoring in case of error
chrootRescueFunction () {
    chrootRestoreAndUmount
}
#
chrootRestoreAndUmount () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    echo -e "$(echoBoldWhiteOnBlue $CHROOT_ENV' restoring files ...')\n"
    
    #echo -e "sources.list: $CHROOT_ENV""$(getVar ETC_APT_SOURCES_LIST_PATH)""\n"
    # restore DNS and sources.list
    if [[ -f $CHROOT_ENV/etc/resolv.conf ]]
    then
        rm $CHROOT_ENV/etc/resolv.conf
    fi
    if [[ -f $CHROOT_ENV/etc/apt/sources.list ]]
    then
        rm $CHROOT_ENV/etc/apt/sources.list
        cp $(getPath4FileInBackupDir sources.list) $CHROOT_ENV"$(getVar ETC_APT_SOURCES_LIST_PATH)"
    fi
    #grep $CHROOT_ENV/dev/pts /proc/mounts
    for mountPoint in dev/pts dev proc sys
    do 
        if grep -qs $CHROOT_ENV/$mountPoint /proc/mounts; then
        echo -e "\n""$(echoBoldWhiteOnYellow $CHROOT_ENV/$mountPoint' is mounted - unmounting.')\n"
        umount "$CHROOT_ENV/$mountPoint"
        else
            #echo "$CHROOT_ENV/$mountPoint was not mounted."
            echo -e "$(echoBoldWhiteOnYellow $CHROOT_ENV/$mountPoint' was not mounted - nothing to do!')\n"
    
        fi
    done
    echo -e "\n"
    return


    # if grep -qs $CHROOT_ENV/dev/pts /proc/mounts; then
    #     echo "$CHROOT_ENV/dev/pts is mounted."
    # else
    #     echo "$CHROOT_ENV/dev/pts ss not mounted."
    # fi

    # # [[ grep -qs $CHROOT_ENV/dev/pts /proc/mounts ]] && echo "$CHROOT_ENV/dev/pts is mounted." || "$CHROOT_ENV/dev/pts ss not mounted."

    # return 

    # [[ -f  "$CHROOT_ENV"/dev/pts ]] && echoBoldWhiteOnRed "unmounting dev pts " && echo -e "\n" &&  umount "$CHROOT_ENV"/dev/pts
    # #
    # [[ -f  "$CHROOT_ENV"/dev ]] && echoBoldWhiteOnRed "unmounting dev " && echo -e "\n" &&  umount "$CHROOT_ENV"/dev
    # #
    # [[ -f  "$CHROOT_ENV"/proc ]] && echoBoldWhiteOnRed "unmounting proc " && echo -e "\n" &&  umount "$CHROOT_ENV"/proc
    # #
    # [[ -f  "$CHROOT_ENV"/sys ]] && echoBoldWhiteOnRed "unmounting sys " && echo -e "\n" &&  umount "$CHROOT_ENV"/sys
    # # echoBoldWhiteOnRed "unmounting dev pts " && echo -e "\n"
    # # umount "$CHROOT_ENV"/dev/pts

    # # echoBoldWhiteOnRed "unmounting dev " && echo -e "\n"
    # # umount "$CHROOT_ENV"/dev
    # # #
    # # echoBoldWhiteOnRed "unmounting proc " && echo -e "\n"
    # # umount "$CHROOT_ENV"/proc
    # # #
    # # echoBoldWhiteOnRed "unmounting sys " && echo -e "\n"
    # # umount "$CHROOT_ENV"/sys
}
#
chrootMountCheck () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local -a mountPoints
    readarray -d $'\n' -t mountPoints < <(grep "$WORK_DIR" /proc/mounts)
    echo -e "Anzahl Mountpoints: ${#mountPoints[@]}""\n"
    for var in "${mountPoints[@]}"
    do
        y=$(echo $var | awk '{print $2}')
        echo $y
    done
    return

    #Array=($( cat /proc/mounts | grep $WORK_DIR ))
    Array=($( grep --null -HRl 'live-iso' /proc/mounts ))
    echo -e "Anzahl: ${#Array[@]}""\n"
    declare -p Array
    return

    if grep -qs $WORK_DIR /proc/mounts; then
        echo "It's mounted."
    else
        echo "It's not mounted."
    fi
}
#
chrootCheckAptPolicy () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    chrootPrepareAndMount # Ressourcen des Build-Systems in Live-System hineinmappen  
    # chroot /home/mayank/chroot/codebase /bin/bash <<"EOT"
    # cd /tmp/so
    # ls -l
    # echo $$ 
    # EOT
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    chroot "$CHROOT_ENV" /bin/bash << "EOT"
#ls && echo -e "\n"
echo -e "pwd: " $(pwd) "\n"
echo "'dollardollar' $$"
# echo -e "checking existence of resolv.conf"
# [[ ! -f /etc/resolv.conf ]] && echo "does not exist" 
# [[ -f /etc/resolv.conf ]] && echo -e "cat resolv.conf" && $(cat /etc/resolv.conf)
echo -e \"apt policy\"
apt policy
# cat /etc/apt/sources.list
# echo -e \"going to exit the bash shell in chroot NOW!\"
exit
EOT

    chrootRestoreAndUmount 
    #
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
}
#
backupIsoFiles () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    for file in $(getVar "UEFI_GRUB_CFG") $(getVar "ISOLINUX_CFG") 
    do
        echo $file
        __backupImpl $file 
        local backupResult=$?
        echo -e "backup result: $backupResult\n"
    done
}
#
backupPngFiles () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    for file in $(getVar "ISOLINUX_PNG_640x480") $(getVar "ISOLINUX_PNG_800x600") 
    do
        echo $file
        __backupImpl $file 
        local backupResult=$?
        echo -e "backup result: $backupResult\n"
    done
}
__create_README () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "creating README in $1 with text: $2"
    ( set -o noclobber
    { echo "$2" > $1 ; } &> /dev/null
    )
    #{ echo "splash.png is original link to ../../isolinux/splash800x600.png" > $(getVar "UEFI_GRUB_DIR")/README ; } &> /dev/null
}
#
fs_in_work-backupSomeFiles () {
    # local PATH_SOURCES_LIST="$(getChrootEnv)""$(getVar ETC_APT_SOURCES_LIST_PATH)"
    # echo -e "$PATH_SOURCES_LIST""\n"
    #return
    local connmanPath="$(getChrootEnv)$(getVar ETC_CONNMAN_CONF_PATH)"
    for file in $(getChrootEnv)/etc/apt/sources.list $connmanPath 
    do
        echo $file
        __backupImpl $file 
        local backupResult=$?
        echo -e "backup result: $backupResult\n"
    done
}
#
check4RootUser () {
    #echo "$1"
    if [[ $EUID -ne 0 ]]
    then
        echo -e "Funktion  $(echoBoldWhiteOnMagenta  $1) $(echoBoldWhiteOnRed 'requires the root user')""\n"
        exit
    fi
}
#
check4User () {
    echo "$1"
    if [[ $EUID -eq 0 ]]
    then
        echo -e "Funktion  $(echoBoldWhiteOnMagenta  $1)  $(echoBoldWhiteOnRed "shouldn't be run as root user!")""\n"
        exit
    fi

}

showEUID () {
    [[ $EUID -ne 0 ]] && echo "$EUID :$USER" || echo -e "$EUID : ROOT" 
}

#
unpackImmutableSourceIsoImage () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local ISO_FILE="$(getVar DEBIAN_ISO)"
    echo -e "$(chattr -i -V $ISO_FILE)""\n"
    trap "chattr +i -V $ISO_FILE" EXIT
    __unpackMutableSourceIsoImage $ISO_FILE
    __checkUnpackedSourceIsoImage $ISO_FILE
    echo -e "$(chattr +i -V $ISO_FILE)""\n"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) erfolgreich abgeschlossen\n"
}
#
__unpackMutableSourceIsoImage () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    # Variable setzen
    #local ISO_FILE="$(getVar DEBIAN_ISO)"
    local ISO_FILE=$1
    # 2022-04-25 make mutable
    local TARGET_DIR="$(getVar ISO_BASE_DIR)"
    local HOME_DIR="$(getVar HOME_DIR)"
    #
    echo -e "Directory at start: $getV$HOME_DIR"
    echo -e "  Quellverzeichnis: $ISO_FILE"
    echo -e "   Zielverzeichnis: $TARGET_DIR"
    echo -e "\n"
    echo -e "Auspacken\n von $ISO_FILE \n nach $TARGET_DIR\n"
    #echo "Bestehendes iso-Verzeichnis wird gelöscht."
    echo "====================================="
    echo
    #set -o xtrace # == set -x
    if [[ -d $TARGET_DIR ]]
     then 
        echo -e "vorheriges $TARGET_DIR wird jetzt gelöscht\n"
        rm -rf "$TARGET_DIR"
    fi
    echo -e "     neues $TARGET_DIR wird jetzt angelegt"
    echo -e "$(mkdir -p $TARGET_DIR)"
    #
    local MNT_DIR="$(getVar ISO_BASE_MOUNT_DIR)"
    echo "ISO_BASE_MOUNT_DIR: $MNT_DIR"
    #echo -e "\n"
    #
    local MNT_DEV=$(losetup -P -f --show "$ISO_FILE")
    echo -e "\n      MNT_DEV: $MNT_DEV\n"
    #
    mount "$MNT_DEV" "$MNT_DIR"
    echo -e "\n"
    cd /"$MNT_DIR"/ || exit
    cp -r . "$TARGET_DIR"
    sync
    echo -e "\n"
    echo -e "$ISO_FILE : Alles ausgepackt\n"
    cd "$HOME_DIR" || exit
    umount "$MNT_DIR"
    losetup -d "$MNT_DEV"
    echo "$MNT_DIR unmounted und $MNT_DEV gelöscht"
    echoBoldWhiteOnGreen "backing up relevant files (just in case we need them later for manual repair ;-)" && echo -e "\n"


    # read -r -p "       vor backup - weiter? [y/N] " questionResponse
    # echo
    # if [[ $questionResponse != [yJ] ]]
    # then
    #     exit
    # fi
    #
    backupIsoFiles
    local backupResult=$?
    echo -e "backupIsoFiles result: $backupResult\n"
    #$(backupFilesFromISOUnpack) &&  echoBoldWhiteOnGreen "done" || (echo -e "\n" && echoBoldWhiteOnRed "backup problem") && echo -e "\n"
    #&&  echoBoldWhiteOnGreen "Files are equal." || (echo -e "\n" && echoBoldWhiteOnRed "Differences!") && echo -e "\n"
    #echo -e "backup result: $backupResult"
    backupPngFiles
    local backupResult=$?
    echo -e "backupPngFiles result: $backupResult\n"
    #
    # create README in boot/grub:
    __create_README $(getVar "UEFI_GRUB_DIR")/README.splash_png "splash.png is original link to ../../isolinux/splash800x600.png" 
    #
    __firmware_replace_links_by_files
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) erfolgreich abgeschlossen\n"
}

__firmware_replace_links_by_files() {
    local iso_dir=$(getVar ISO_BASE_DIR)
    echo -e "iso_dir: $iso_dir"
    local firmware_dir=$(getVar ISO_FIRMWARE_DIR)
    echo -e "firmware in: ${firmware_dir}"
    helper_folder=/tmp/firmware_helper
    if [ -d $helper_folder ]; then
        rm -rf $helper_folder
    fi
    mkdir -p $helper_folder
    (cd $iso_dir && 
        cp -L -r firmware/ $helper_folder/ && 
        rm -rf firmware && 
        mv $helper_folder/firmware .
    )
}

testReadMe () {
    # __create_README $(getVar "UEFI_GRUB_DIR")/README "splash.png is original link to ../../isolinux/splash800x600.png" 
    # #__create_README $(getVar "UEFI_GRUB_DIR") "Text für das README"
    __create_README $(getVar "UEFI_GRUB_DIR")/README.splash_png "splash.png is original link to ../../isolinux/splash800x600.png" 

}


__checkUnpackedSourceIsoImage () {
    #
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    # Variable setzen
    local ISO_FILE=$1
    #local ISO_FILE="$(getVar DEBIAN_ISO)"
    local TARGET_DIR="$(getVar ISO_BASE_DIR)"
    local HOME_DIR="$(getVar HOME_DIR)"
    #
    echo -e "Patience - check consistency of unpacked files!"
    echo -e "directory at start: $HOME_DIR"
    echo -e "  source directory: $ISO_FILE"
    echo -e "  target directory: $TARGET_DIR"
    echo -e "\n"
    echo -e "Vergleiche die ausgepackten ISO-Files mit $ISO_FILE\n"
    echo "====================================="
    echo
    #set -o xtrace # == set -x
    #
    local MNT_DIR="$(getVar ISO_BASE_MOUNT_DIR)"
    echo "ISO_BASE_MOUNT_DIR: $MNT_DIR"
    #echo -e "\n"
    #
    local MNT_DEV=$(losetup -P -f --show "$ISO_FILE")
    echo -e "\n      MNT_DEV: $MNT_DEV\n"
    #
    mount "$MNT_DEV" "$MNT_DIR"
    echo -e "\n"

    cd /"$MNT_DIR"/ || exit
    #
    FILE1=/tmp/dir1.txt
    FILE2=/tmp/dir2.txt
    echo -e "$FILE1\n"
    echo -e "$FILE2\n"
    #
    rm -f $FILE1
    rm -f $FILE2
    #
    date
    #
    echo "Start: checksumming $MNT_DIR"
    cd "$MNT_DIR" && find . -type f -exec md5sum {} + | sort -k 2 > "$FILE1"
    #
    cd "$HOME_DIR" || exit
    echo "... bin wieder in $(pwd)"
    umount "$MNT_DIR"
    losetup -d "$MNT_DEV"
    echo "$MNT_DIR unmounted"
    # fertig mit original
    date
    echo "Now: Start checksumming $TARGET_DIR"
    cd "$TARGET_DIR" || exit
    echo "$(pwd), untersuche iso-Verzeichnis"
    find . -type f -exec md5sum {} + | sort -k 2 > "$FILE2"
    cd "$HOME_DIR" || exit
    date
    echo -e "Unterschiede feststellen ...\n"
    #return
    diff --context=1 $FILE1 $FILE2 &&  echoBoldWhiteOnGreen "Files are equal." || ((echo -e "\n" && echoBoldWhiteOnRed "Differences!") && exit ) && echo -e "\n"
    echo -e "\nVergleich beendet\n"
    #
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) erfolgreich abgeschlossen\n"

}
#
fs_in_work-unpackSquashfs () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local sourcePathSquashfs="$(getVar ISO_BASE_DIR)"/live/filesystem.squashfs
    echo -e "ISO: $sourcePathSquashfs\n"
    #[ ! -d "$WORK_DIR" ] && echo "$WORK_DIR is missing - check!" && exit
    #[ -f "$sourcePathSquashfs" ] ||  "$(echoBoldWhiteOnRed $sourcePathSquashfs' is missing!')" && exit 
    ## ok !! [ ! -f "$sourcePathSquashfs" ] && echo "$sourcePathSquashfs is missing  - check!" && exit
    [ ! -f "$sourcePathSquashfs" ] && echo -e "$(echoBoldWhiteOnRed $sourcePathSquashfs' is missing  - check!')\n" && exit
    local targetSquashfs="$(getVar BASE_DIR)"/fs_in_work
    echo -e "TARGET: $targetSquashfs\n"
    local HOME_DIR="$(getVar HOME_DIR)"
    #
    if [[ -d "$targetSquashfs" ]]
    then
        echo -e "$(echoBoldWhiteOnRed $targetSquashfs' is already present')""\n"
        echo "              ====================================="
        echo 
        read -r -p "       going to delete $targetSquashfs now? [y/N] " questionResponse
        echo
        if [[ $questionResponse != [yJ] ]]
        then
	        exit
        else 
            rm -rf "$targetSquashfs" 
        fi
    else 
        echo "going on"
    fi
    #return
    #echoBoldWhiteOnYellow "Diese Funktion löscht das Zielverzeichnis $targetSquashfs!"
    echo
    echo
    #echo -e "Directory at start: $HOME_DIR"
    #echo
    #
    set -o xtrace
    trap "set +o xtrace" RETURN
    #
    date
    #
    unsquashfs -d "$targetSquashfs" "$sourcePathSquashfs"
    #
    date
    #
    #set +o xtrace
    #
    # backup important files
    fs_in_work-backupSomeFiles
    local res=$?
    echo -e "Backup Result: $res""\n"
    cd "$HOME_DIR" || exit
    #
    # squashfsSetNewKernelPrefs
    # local addRes=$?
    # echo -e "squashfsSetNewKernelPrefs Result: $addRes""\n"
    fs_in_work-setup__kernelprefs
    #
    # set symlink
    local targetSquashfsLink="$WORK_DIR"/fs_in_work
    $(ln -s "$targetSquashfs" "$targetSquashfsLink")
    date
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen ended successfully)""\n"
}

# getAptPurgeList () {
#     echo "\
#     libreoffice-* \
#     blue* \
#     k3b* \
#     vlc* \
#     fonts-noto-cjk \
#     git* \
#     calamares* \
#     lvm2 \
#
#     cryptsetup \
#     smbclient \
#     libsmbclient
#     "
# }


fs_in_work-purgeUnwanted () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "This function removes unwanted packages from live-iso""\n"
    # Ressourcen des Build-Systems in Live-System hineinmappen
    chrootPrepareAndMount
    #
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    chroot "$CHROOT_ENV" /bin/bash <<"EOT"
echo $$ 
echo "Hallo"
#cat /etc/resolv.conf
apt update 
apt purge -y \
    libreoffice-* \
    blue* \
    k3b* \
    vlc* \
    fonts-noto-cjk \
    git* \
    calamares* \
    lvm2 \
    btrfs* \
    cryptsetup \
    smbclient \
    libsmbclient
apt -y purge firefox-esr-*
apt -y clean
apt -y autoremove --purge 
apt install firefox-esr-l10n-de


EOT
    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
    return

}



fs_in_work-update-grub2 () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "This function updates the initrd.img""\n"
    # Ressourcen des Build-Systems in Live-System hineinmappen
    chrootPrepareAndMount
    #
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"

    local TARGET="$(getChrootEnv)"
    # initramfs updatebar machen
    #
    chroot "$TARGET" /usr/sbin/update-grub2
    #     #
    echo -e "... leaving now"
    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
    return
}

fs_in_work-update-plymouth-theme () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "This function updates the initrd.img""\n"
    # Ressourcen des Build-Systems in Live-System hineinmappen
    chrootPrepareAndMount
    #
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"

    local TARGET="$(getChrootEnv)"
    # initramfs updatebar machen
    #
    chroot "$TARGET" mv /usr/sbin/update-initramfs /usr/sbin/update-initramfs.old
    chroot "$TARGET" ln -s /usr/sbin/update-initramfs.orig.initramfs-tools /usr/sbin/update-initramfs
    #chroot "$TARGET" /usr/bin/update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/homeworld/homeworld.plymouth 100
    #chroot "$TARGET" /usr/bin/update-alternatives --config default.plymouth
    chroot "$TARGET" /usr/sbin/plymouth-set-default-theme -R homeworld
    chroot "$TARGET" /usr/sbin/update-initramfs -u
    chroot "$TARGET" rm /usr/sbin/update-initramfs
    chroot "$TARGET"  ln -s /bin/live-update-initramfs /usr/sbin/update-initramfs
    #     #
    echo -e "... leaving now"
    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
    return
}
#
fs_in_work-setup_plymouth_splash () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local sourcePath="$(getVar PLYMOUTH_SPLASH_RESOURCE_PNG)"
    echo -e "$sourcePath""\n"
    [ ! -f "$sourcePath" ] && echo "$sourcePath is missing!" && return
    local targetPath="$(getVar PLYMOUTH_SPLASH_TARGET_PNG)"
    echo -e "$targetPath""\n"

    /usr/bin/cp "$sourcePath" "$targetPath"
    local copyResult="$?"
    echo "copyResult $copyResult"  

    [[ $copyResult == 0 ]] && echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen $sourcePath' copied to fs_in_work')\n"

}
#
fs_in_work-manage-plymouth () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #
    fs_in_work-setup_plymouth_splash
    fs_in_work-update-plymouth-theme
}
#
fs_in_work-update-initramfs () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "This function updates the initrd.img""\n"
    # Ressourcen des Build-Systems in Live-System hineinmappen
    chrootPrepareAndMount
    #
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"

    local TARGET="$(getChrootEnv)"
    # initramfs updatebar machen
    #
    chroot "$TARGET" mv /usr/sbin/update-initramfs /usr/sbin/update-initramfs.old
    chroot "$TARGET" ln -s /usr/sbin/update-initramfs.orig.initramfs-tools /usr/sbin/update-initramfs
    chroot "$TARGET" /usr/sbin/update-initramfs -u
    chroot "$TARGET" rm /usr/sbin/update-initramfs
    chroot "$TARGET"  ln -s /bin/live-update-initramfs /usr/sbin/update-initramfs
    #     #
    echo -e "... leaving now"
    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
    return
}
#
########## <<<<<<<<<<<<<<<<<<<< KEEP THIS for inspiration -> todo list >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><
#     #### DAS erst SPÄTER !!
#     # initramfs updatebar machen
#     #
#     ### chroot "$TARGET" mv /usr/sbin/update-initramfs /usr/sbin/update-initramfs.old
#     ### chroot "$TARGET" ln -s /usr/sbin/update-initramfs.orig.initramfs-tools /usr/sbin/update-initramfs
#     #
########## >>>>>>>>>>>>>>>>>>>  KEEP stuff above for inspiration -> todo list <<<<<<<<<<<<<<<<<<<<<<<<<<<<

fs_in_work-installNewPackages () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "This function installs additional packages (uses update and dist-upgrade). " "\n"
    # read the list from config file
    myPackageList=$(__printArry ' ' "${X_myPackages[@]}")
    # Ressourcen des Build-Systems in Live-System hineinmappen
    chrootPrepareAndMount
    #
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    chroot "$CHROOT_ENV" /bin/bash <<"EOT"
echo $$ 
echo "Hallo"
#cat /etc/resolv.conf
#cat /etc/apt/sources.list
apt update
apt policy 
apt dist-upgrade -y
apt -y autoremove --purge    
apt -y clean
EOT


    # chroot "$CHROOT_ENV"/ apt-get -y install \
    #    tzdata task-german squashfs-tools cups wswiss wngerman wogerman aspell-de \
    #    hunspell-de-de vim dnsutils mlocate mc colordiff  \
    #    apt-file psmisc tree \
    #    firmware-iwlwifi wpasupplicant iw rfkill wireless-tools \
    #    firmware-amd-graphics firmware-realtek firmware-linux \
    #    intel-microcode iucode-tool thermald \
    #    ausweisapp2

    chroot "$CHROOT_ENV"/ apt-get -y install ${myPackageList}

DEBIAN_FRONTEND=noninteractive chroot "$CHROOT_ENV"/ apt-get -y install tshark 


    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
}


# __load_debian_package_list() {
#     echo -e "Arguments: $#"
#     echo -e "$1"
#     #return
#     # echo -e " working "

#     # #set -o nounset

#     # #packagefile="${X_PACKAGE_FILE}"

#     # initialize the package variable
#     #packages=''
#     packages=''

#     # read the lines of the package file
#     while IFS= read -r line; do
#         packages+=" $line"
#     #done < $packagefile
#     #done < "${X_PACKAGE_FILE}"
#     done < "$1"

#     # # apt install all of the packages
#     # echo -e "\n"
#     # echo -e "my list is:\n"
#     # echo -e $packages"\n"
#     echo $packages
# }

# __load_debian_package_list() {
#     echo -e "Arguments: $#"
#     echo -e "$1"

#     #printf -v string "${X_myPackages}"
#     printf -v string "%s" "$@"
#     echo string
# }

__printArry() {
    local sep=$1 string
    shift
    printf -v string "%s${sep//%/%%}" "$@"
    echo "${string%"$sep"}"
}        


fs_in_work_load_debian_package_list() {

    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "This function loads the list of additional packages from the config file ... " "\n"
    myPackageList=$(__printArry ' ' "${X_myPackages[@]}") #"${ids[@]}"
    echo -e "${myPackageList}""\n"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
}


fs_in_work-upgradePackages () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "This function dist-upgrades all packages." "\n"
    # Ressourcen des Build-Systems in Live-System hineinmappen
    chrootPrepareAndMount
    #
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    chroot "$CHROOT_ENV" /bin/bash <<"EOT"
echo $$ 
echo "Hallo"
#cat /etc/resolv.conf
#cat /etc/apt/sources.list
apt update
apt policy 
apt dist-upgrade -y
apt -y autoremove --purge    
apt -y clean
EOT

    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
}

test_kernelUpgrade () {
#
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local CHROOT_ENV="$(getChrootEnv)"
    chrootPrepareAndMount
    #
    chroot "$CHROOT_ENV" /bin/bash <<"EOT"
echo $$ 
echo "check for kernels"
#ls | grep .deb | tr '\n' ' ' | xargs dpkg -i
#rm *.deb
list=$(dpkg-query -W -f='${binary:Package} ' 'linux-image*' 'linux-headers*' 'linux-libc*')
echo $list
declare -a kernels
kernels=($list)
echo "${#kernels[@]} kernel"
for i in "${kernels[@]}"; do echo $i && echo -e "\n"; done
EOT
#

    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 


}

test__KernelVersionRef () {

    local -n reference="$1"
    reference=$(getVar "KERNEL_VERSION")
}

__deleteAllElementsOfArrayTwoInArrayOne () {
## see also function __removeFromArray
    # https://stackoverflow.com/questions/16860877/remove-an-element-from-a-bash-array
    local -n array="$1"
    local -n keepPackage="$2"
    for package in "${keepPackage[@]}"; do
        for i in "${!array[@]}"; do
            if [[ ${array[i]} = $package ]]; then
                unset 'array[i]'
            fi
        done
    done
}

chroot_JobFunction_removeElderKernels () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"  
    IFS='|' read -r -a keepKernels <<< "$package2Keep"
    echo "keepKernels:"
    for i in "${keepKernels[@]}"
        do 
            echo $i
    done
    list=$(dpkg-query -W -f='${binary:Package} ' 'linux-image*' 'linux-headers*')
    declare -a packageArray
    packageArray=($list)
    __deleteAllElementsOfArrayTwoInArrayOne packageArray keepKernels
    for package in "${packageArray[@]}"
        do 
            echo $package
            read -r -p "    remove package $package - [y/N] " questionResponse
            echo
            if [[ $questionResponse != [yJ] ]]
            then
                echo "not removing $package" 
            else
                apt-get -y --purge remove $package 
            fi
    done 
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}
#
fs_in_work-getRidOfElderKernels () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local kernelVersion=$(getVar "KERNEL_VERSION")
    echo -e "current kernelVersion: $kernelVersion""\n" 
    export package2Keep="linux-headers-686-pae|linux-image-generic|linux-headers-generic|linux-headers-amd64|linux-headers|linux-image-amd64|linux-headers-${kernelVersion}|linux-image-${kernelVersion}"
# https://stackoverflow.com/questions/30792028/executing-function-inside-chroot-in-bash
    export -f chroot_JobFunction_removeElderKernels
    export -f echoBoldWhiteOnMagenta setTextBold setTextWhite setBackgroundMagenta resetText echoBoldWhiteOnGreen setBackgroundGreen
    export -f __deleteAllElementsOfArrayTwoInArrayOne 
    local CHROOT_ENV="$(getChrootEnv)"
    chrootPrepareAndMount   
    chroot "$CHROOT_ENV" /bin/bash -c "chroot_JobFunction_removeElderKernels"
    chrootRestoreAndUmount 
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}

# test_executeHelperScript () {
#     local CHROOT_ENV="$(getChrootEnv)"
#     local HELPER_SCRIPT_PATH="$CHROOT_ENV"/tmp/helper.sh
#     chrootPrepareAndMount 
#     #
#     chroot "$CHROOT_ENV" /bin/bash << "EOT"
# #ls && echo -e "\n"
# echo -e "pwd: " $(pwd) "\n"
# echo "'dollardollar' $$"
# cat /tmp/helper.sh    
# echo -e "\n""executing now:""\n"  
# source /tmp/helper.sh
# #
# EOT
#
#    chrootRestoreAndUmount 
#     #rm "$HELPER_SCRIPT_PATH" && echo -e "$(echoBoldWhiteOnBlue '$HELPER_SCRIPT_PATH removed')" 
#     #
#     echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
# }
#
#
fs_in_work-upgradeKernel () {
    #trap 'listFunctions' ERR
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"

    fileGlob="$(getVar KERNEL_PACKAGE_DIR)/linux*$(getVar KERNEL_VERSION)*.deb"
    if ! (compgen -G "$fileGlob")
    then 
        echo "No Kernel Upgrade"
        upgradeKernel=1
    else
        echo "Jetzt geht es los"
        upgradeKernel=0 
    fi

    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"


    echo -e "This function updates the kernel packages." "\n"
    # Ressourcen des Build-Systems in Live-System hineinmappen
    # Dateien kopieren
    for f in $fileGlob
    do
        echo "File -> $f"
        cp "$(getVar KERNEL_PACKAGE_DIR)"/linux*.deb "$CHROOT_ENV"/
    done
 
    chrootPrepareAndMount
    #
    chroot "$CHROOT_ENV" /bin/bash <<"EOT"
echo $$ 
echo "installation of new kernel"
ls | grep .deb | tr '\n' ' ' | xargs dpkg -i
rm *.deb
list=$(dpkg-query -W -f='${binary:Package} ' 'linux-image*')
echo $list
declare -a kernels
kernels=($list)
echo "${#kernels[@]} kernel"
for i in "${kernels[@]}"; do echo $i && echo -e "\n"; done
EOT
#

    isoLiveDir_UpgradeBootFiles
    changeISOBootIsolinuxCfg
    changeUefiBootGrubCfg
    #
    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
}

checkChrootenv4Firefox () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "- - - - - - - - - - - - - - "
    chrootPrepareAndMount
    #
    local CHROOT_ENV="$(getChrootEnv)"
    chroot "$CHROOT_ENV" /bin/bash <<"EOT"
echo $$ 
echo "check for firefox packages"
list=$(dpkg-query -W -f='${binary:Package} ' 'firefox*')
echo $list
declare -a foxes
foxes=($list)
echo "${#foxes[@]} firefox package(s)"
for i in "${foxes[@]}"; do echo $i && dpkg -l $i && echo -e "\n"; done
EOT
    chrootRestoreAndUmount
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
}

checkBashVersion () {

    #compgen -v
    #compgen -A variable
    for n in 0 1 2 3 4 5
    do
        echo "BASH_VERSINFO[$n] = ${BASH_VERSINFO[$n]}"
    done  
    cat << "_end_of_text"
#
# explanation:
#
# BASH_VERSINFO[0] = 3                      # Major version no.
# BASH_VERSINFO[1] = 00                     # Minor version no.
# BASH_VERSINFO[2] = 14                     # Patch level.
# BASH_VERSINFO[3] = 1                      # Build version.
# BASH_VERSINFO[4] = release                # Release status.
# BASH_VERSINFO[5] = i386-redhat-linux-gnu  # Architecture
                              
                                            # (same as $MACHTYPE).
_end_of_text


# ( set -o posix ; set )

}

#
### Schrott-Areal
#     chroot "$CHROOT_ENV" /bin/bash <<"EOT"
# echo $$ 
# echo "Hallo"
# #ls
# # ls | grep .deb
# # ls | grep .deb | tr '\n' ' ' | xargs dpkg -i
# # rm *.deb
# #dpkg --list | grep linux-image | tr '\n' ' ' | xargs echo
# #list=$(dpkg --list | grep linux-image )
# #echo $list
# # echo "Wilhelm ist hier"
# list=$(dpkg-query -W -f='${binary:Package} ' 'linux-image*')
# echo $list
# declare -a kernels
# kernels=($list)
# echo "${#kernels[@]} kernel"
# # #
# # #declare -a candidates
# # echo -e "xxx""\n"
# for i in "${kernels[@]}"; do echo $i && echo -e "\n"; done
# # #dpkg --list | grep -i -E --color 'linux-image|linux-kernel' | grep '^ii'
# # #
# # #apt -y remove --purge linux-image-5.15.0-0.bpo.3-amd64
# # #
# #apt install byobu
# # #purge-old-kernels -y
# # #purge-old-kernels --keep 3 -y 
# # ### apt --purge -y autoremove 
# # #dpkg --list | grep -i -E --color 'linux-image|linux-kernel' | grep '^ii'
# # echo "Done in CHROOT"
# # #cat /etc/resolv.conf
# # #cat /etc/apt/sources.list
# # #apt update
# # #apt policy 
# # #apt dist-upgrade -y
# # #apt -y autoremove --purge    
# # #apt -y clean
# EOT

#
__testCompgen () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})"
    fileGlob="$(getVar KERNEL_PACKAGE_DIR)/linux*.deb"
    #result=$(compgen -G "$fileGlob")
    if ! (compgen -G "$fileGlob"); then echo "Ha"; fi
    local res=$?
    echo "res: $res"
    echo $result
}
#
upgradeFilesystemSquashFS () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    #fileGlob="$(getVar KERNEL_PACKAGE_DIR)/linux*.deb"
    #[[ -f "$(getVar KERNEL_PACKAGE_DIR)/linux*.deb" ]] && echo "kernel packages available"
    #ls -l "$(getVar KERNEL_PACKAGE_DIR)/linux*.deb"
    # 
    # is a new kernel provided
    # be optimistic
    local upgradeKernel=0
    echo -e "Kernel Version is $(getVar KERNEL_VERSION)""\n"
    fileGlob="$(getVar KERNEL_PACKAGE_DIR)/linux*$(getVar KERNEL_VERSION)*.deb"
    if ! (compgen -G "$fileGlob")
    then 
        echo "No Kernel Upgrade"
        upgradeKernel=1
    else
        echo "Jetzt geht es los"
        upgradeKernel=0 
    fi
    echo -e "upgrateKernel: $upgradeKernel""\n"
    if [ "$upgradeKernel" == "0" ] ; then echo "Kernel upgrade" ; fi
    return

    for f in $(getVar KERNEL_PACKAGE_DIR)/linux*.deb #"$(ls -l $fileGlob)"
    do
        echo "File -> $f"
    done
}
#    #
    # chrootPrepareAndMount # Ressourcen des Build-Systems in Live-System hineinmappen
    # #chrootRestoreAndUmount 
    # ### CHECK THIS!
    # # DNS + Paketquellen des Build-Systems nutzen, vorher Ressourcen des Live-Systems sichern
    # # 2022-03-09 : iso-live braucht scheinbar kein resolv.conf
    # cp -dp /etc/resolv.conf /etc/resolv.conf.original
    # cp -dp /etc/apt/sources.list /etc/apt/sources.list.original
    # cp /etc/resolv.conf "$CHROOT_ENV"/etc/
    # #
    # chroot "$CHROOT_ENV" mv /usr/sbin/update-initramfs /usr/sbin/update-initramfs.old
    # chroot "$CHROOT_ENV" ln -s /usr/sbin/update-initramfs.orig.initramfs-tools /usr/sbin/update-initramfs
    # # alle Updates einspielen
    # chroot "$CHROOT_ENV"/ apt-get update
    # chroot "$CHROOT_ENV"/ apt-get -y dist-upgrade
    # chroot "$CHROOT_ENV"/ apt-get -y check
    # chroot "$CHROOT_ENV"/ apt-get -y autoremove --purge
    # chroot "$CHROOT_ENV"/ apt-get -y clean
    # #
    # # wjp 2022-02-16 eigenen Kernel einbauen
    # # nach ctbankix-Rezept
    # # Modifizierten Kernel einspielen
    # cp "$(getVar KERNEL_PACKAGE_DIR)"/linux*.deb "$CHROOT_ENV"/
    # chroot "$CHROOT_ENV"/ ls | chroot "$CHROOT_ENV"/ grep .deb | chroot "$CHROOT_ENV"/ tr '\n' ' ' | chroot "$CHROOT_ENV"/ xargs dpkg -i
    # chroot "$CHROOT_ENV"/ apt-get -f -y install
    # rm "$CHROOT_ENV"/*.deb
    # #
    # chroot "$CHROOT_ENV"/ apt-get -y autoremove --purge
    # #
    # #umount "$CHROOT_ENV"/dev/pts "$CHROOT_ENV"/dev "$CHROOT_ENV"/proc "$CHROOT_ENV"/sys
    # chrootRestoreAndUmount 
    # # DNS + Paketquellen des Build-Systems nutzen, vorher Ressourcen des Live-Systems sichern
    # cp -dp /etc/resolv.conf.original /etc/resolv.conf 
    # cp -dp /etc/apt/sources.list.original /etc/apt/sources.list 
    # #
    # chroot "$CHROOT_ENV"/ rm etc/resolv.conf
    # #
    # cd "$BASE_DIR" || exit
    # #erzeuge neues Squash-Filesystem
    # #mksquashfs "$CHROOT_ENV" new.debian.filesystem.squashfs -noappend -comp lz4 -Xhc
    # echo "$(basename "$0"):$CHROOT_ENV - Update abgeschlossen"

#
replaceFilesystemSquashfs () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #
    # a new kernel needs a new filesystem.squashfs with it's boot directory
    # so: all files iso/live/* can be removed in one step
    #
    local filesystemSquashfsPath=$(getVar ISO_LIVE_DIR)/filesystem.squashfs
    #echo "target dir: $liveDir"
    local newSquashfsPath=$(getVar BASE_DIR)/$(getVar SQUASHFS_NEW_FILE)
    #$(sha512sum $newSquashfsPath) > newSquashfsPath.sha512
    echo -e "copy $newSquashfsPath to $filesystemSquashfsPath""\n"
    cp $newSquashfsPath $filesystemSquashfsPath && echo -e "$(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen successfully copied - done)""\n" 
}
#
#createNewFileystemSquashfs () {
fs_in_work-MakeNewSquashfs () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    cd "$(getVar BASE_DIR)" || exit
    echo $(pwd)
    #
    local NEW_SQUASHFS="$(getVar SQUASHFS_NEW_FILE)"
    local SQUASHFS_DIR="$(getVar FS_IN_WORK_DIR)"
    echo -e "x: $SQUASHFS_DIR""\n"
    echo "***********************************"
    echo "$SQUASHFS_DIR squashed to  $NEW_SQUASHFS"
    [ -f $NEW_SQUASHFS ] && echo -e "$(echoBoldWhiteOnYellow $NEW_SQUASHFS' exists - it will be deleted and renewed.')""\n"
    echo "====================================="
    echo
    read -r -p "start? [y/N] " questionResponse
    echo
    if [[ $questionResponse != [yY] ]]
    then
        exit
    fi
    #set -o xtrace
    mksquashfs $SQUASHFS_DIR $NEW_SQUASHFS -noappend -comp lz4 -Xhc
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
    cd "$(getVar HOME_DIR)" || exit
}
#
createMbrTemplate () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #set -x
    [ ! -d $(getVar BASE_DIR_MBR) ] && echo "masterbootrecord dir not present" && exit
    echo -e "source: $(getVar DEBIAN_ISO)""\n"
    echo -e "target: $(getVar MBR_PATH)""\n"
    dd if="$(getVar DEBIAN_ISO)" bs=1 count=432 of="$(getVar MBR_PATH)"
    echo -e "Function: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen $(getVar MBR_PATH)' is created')""\n" 
   
}
#
isoCreate () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    unpackIsoAndCreate_fs_in_work
    fs_in_work-SetupPersonalVersion
    fs_in_work-upgradeKernel
    fs_in_work-MakeNewSquashfs
    replaceFilesystemSquashfs
    isoPack
    echo -e "Function: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen 'fs_in_work ready for packing')""\n" 

}
#
isoPack () {
    # based on
    # https://wiki.debian.org/RepackBootableISO
    # 
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local ISO_PREFIX=""
    #local ISO_PREFIX="dk-wjp-2023-09-21_0"
    #local ISO_PREFIX=${OUTPUT_ISO}
    createIsoVersionString ISO_PREFIX
    echo "$ISO_PREFIX"
    #return
    local new_files="$(getVar ISO_BASE_DIR)"
    local new_iso="$(getVar TARGET_DIR_4_NEW_ISO)"/"$ISO_PREFIX".iso
    echo -e "new_files: $new_files""\n"
    echo -e "  new_iso: $new_iso"
    #return
    xorriso -as mkisofs \
        -r -V "$ISO_PREFIX" \
        -o "$new_iso" \
        -J -J -joliet-long \
        -isohybrid-mbr "$(getVar MBR_PATH)" \
        -b isolinux/isolinux.bin \
        -c isolinux/boot.cat \
        -boot-load-size 4 -boot-info-table -no-emul-boot \
        -eltorito-alt-boot \
        -e boot/grub/efi.img \
        -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus \
        "$new_files"
    echo -e "Function: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen $new_iso is created)""\n" 
    createLinkLatestIso
    __createLinkInConfig4RunIso $new_iso

    # original: -J -J -joliet-long -cache-inodes \ 
    #         : -cache-inodes is default and produces anwanted message
}
#
# isoPack_Version () {
#     __isoPack "myNew20220329.0"
# }
#
__check4virt_kvm() {
    isVirt=$(/usr/bin/systemd-detect-virt)
    echo -e "isVirt? $isVirt"
    #echo "$isVirt==kvm"
    if [ "$isVirt" = "kvm" ]; then
        echo 0
    else
        echo 1
    fi
    echo 
}
#
check4virt_kvm() {
    result=$(__check4virt_kvm)
    echo "kvm-Check: $result"
}
#
__check4sshConnection() {
    # https://linuxhint.com/check-if-environment-variable-exists-get-its-value-bash/
    # if [ -z "$SSH_CLIENT" ]
    #     then
    #         echo "Environmental variable does not exist."
    # else
    #     echo "The value of this Environmental Variable is: $SSH_CLIENT"
    # fi
    # return

    # if [ -z "$PATH" ]
    #     then
    #         echo "Environmental variable does not exist."
    # else
    #     echo "The value of this Environmental Variable is: $PATH"
    # fi
    # local ssh_value=$(printenv ${SSH_CLIENT})
    # echo -e "value: $ssh_value"
    # https://unix.stackexchange.com/questions/9605/how-can-i-detect-if-the-shell-is-controlled-from-ssh
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
      SESSION_TYPE=remote/ssh
    # many other tests omitted
    else
        case $(ps -o comm= -p "$PPID") in
            sshd|*/sshd) SESSION_TYPE=remote/ssh;;
        esac
    fi
    #echo -e "SESSION_TYPE=$SESSION_TYPE"
    if [ -z "$SESSION_TYPE" ]
        then
            echo ""
    else
        echo -e "SESSION_TYPE=$SESSION_TYPE"
    fi
 }
#
check4sshConnection() {
    local ssh_check_result=$(__check4sshConnection)
    if [ -z $ssh_check_result ]; then
        echo -e "Desktop"
    else
        echo "$ssh_check_result"
    fi
}
#
__virsh_start_default_virbr0() {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    sudo virsh net-start default
    sudo virsh net-list --all
}
__virsh_stop_default_virbr0() {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    sudo virsh net-destroy default
    sudo virsh net-list --all
}
virsh_list_networks () {
    sudo virsh net-list --all
}
virsh_net() {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "Argument: $1"
    local cmd=__virsh_$1_default_virbr0
    eval $cmd
}
#
#
runNewIso () {
    #check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    # virsh list --all
    local activeNetwork=$(sudo virsh net-list | tail -n +3 | awk '{print $1}')
    echo -e "activeNetwork x$activeNetwork""\n"
    [[ x$activeNetwork == "x" ]] && echo -e "no active network" || echo "active network $activeNetwork" 
    #return
    # return
    source $CONFIG_FILE
    ISO=$X_LATEST_ISO
    echo -e "now running iso: $ISO"
    #return
    #virsh net-start default
    #__virsh_start_default_virbr0
    virsh_net start
    qemu-system-x86_64 -m 12288 -machine accel=kvm -cpu host -netdev bridge,br=virbr0,id=net0 -device virtio-net-pci,netdev=net0 -bios /usr/share/qemu/OVMF.fd -cdrom "$ISO" -smp 4 -vga qxl
    #virsh net-destroy default
    #__virsh_stop_default_virbr0
    virsh_net stop
    echo -e "done""\n"
}
#
# step 0 
makeClean () {
    # https://www.networkworld.com/article/3546252/how-to-find-and-remove-broken-symlinks-on-linux.html
    # find . -xtype l 2>/dev/null -exec rm {} \;
    check4RootUser "${FUNCNAME[0]}"
    #
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo $WORK_DIR
    $(find $WORK_DIR -lname "*" -delete)
    #find $WORK_DIR -xtype l 2>/dev/null -exec rm {} \;
#    return
    local TARGET_DIR="$(getVar BASE_DIR)"
    [ ! -d "$TARGET_DIR" ] && echo "Problem with $(echoBoldWhiteOnRed $TARGET_DIR) - check!!" && exit
    #
    echo -e "make clean: \n $TARGET_DIR""\n"
    echo "              ====================================="
    echo 
    read -r -p "       remove existing $TARGET_DIR now? [y/N] " questionResponse
    echo
    if [[ $questionResponse != [yY] ]]
    then
        echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnRed $TARGET_DIR' bleibt erhalten')""\n" 
	    exit
    fi
    echo -e "Antwort: $questionResponse"
    rm -rf $TARGET_DIR && echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen $TARGET_DIR' removed')""\n" 
}
#
function printVars {
    echo -e "printVars""\n"
    # # # for val in "${var[@]}"; do echo $val; done
    # # # echo -e "printKeys""\n"
    # # # for key in "${!var[@]}"; do echo $key; done
    # for elem in "${!var[@]}"
    # do
    #     echo "key : ${elem}" -- "value: ${var[${elem}]}"
    # done
    # return
    mapfile -d '' sorted < <(printf '%s\0' "${!var[@]}" | sort -z)
    for key in "${sorted[@]}"; do
        printf '   %s\n        %s\n' "$key" "${var[$key]}"
    done
    return
    # for variable in "${!var[@]}"
    # do printf "  %s\n       %s\n" "$variable" "${var[$variable]}"
    # done
    # |
    #sort -rn -k1
    #echo -e "\n"
    #sort -rn -variable1

    # IFS=$'\n' sorted=($(sort <<<"${var[*]}")); unset IFS
    # for variable in "${!sorted[@]}"
    # do printf "  %s\n       %s\n" "$variable" "${sorted[$variable]}"
    # done
    # echo -e "\n"
   
    #printf "%s\n" "${var[@]}"

    #printf "Group file in Linux or Unix: %s\n" "${files[1]}"

}
#
printVarsSorted () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    # #echo $(getChrootEnv)
    # echo "Anzahl Elemente: ${#namesList[@]}"
    # mapfile -d '' sorted < <(printf '%s\0' "${namesList[@]}" | sort -z)
    # for element in ${sorted[@]}
    # do
    #     echo $element
    # done

    printVarByList namesList "names"
    printVarByList dirList "directories"
    printVarByList pathList "paths"

    return
    #__print_variables namesList # var 
}
#
printVarByList () {
    #$(echoBoldWhiteOnBlue "$2")
    #echo -e "\n"
    for variable_name in "$1"; do
        declare -n variable_reference="$variable_name"
        echo -e "\n$(echoBoldWhiteOnBlue $variable_name)"" ""$(echoBoldWhiteOnCyan \($2\))\n"
    done

    mapfile -d '' sorted < <(printf '%s\0' "${variable_reference[@]}" | sort -z)
    #echo -e "xxxx: ${sorted[@]}""\n" 
    for entry in "${sorted[@]}"; do
        printf '   %s\n      %s\n' "$entry" "$(getVar $entry)" 
            #echo "$entry"
    done
}
#
__print_variables () {
    for variable_name in "$@"; do
    declare -n variable_reference="$variable_name"
    #
    echo -e "$variable_name:""\n"
    #
    case "$(declare -p "$variable_name")" in
    "declare -a"* )
        mapfile -d '' sorted < <(printf '%s\0' "${variable_reference[@]}" | sort -z)
        #echo -e "xxxx: ${sorted[@]}""\n" 
        for entry in "${sorted[@]}"; do
            printf '   %s\n' "$entry"
            #echo "$entry"
        done
      ;;
    "declare -A"* )
        mapfile -d '' sorted < <(printf '%s\0' "${!variable_reference[@]}" | sort -z)
        #echo -e "xxx: ${sorted[@]}""\n"
        for key in "${sorted[@]}"; do
            printf '   %s\n        %s\n' "$key" "${variable_reference[$key]}"
            #echo -n " $key=\"${variable_reference[$key]}\""
      done
      ;;
    * )
      echo -n " $variable_reference"
      ;;
    esac
    #
    echo
  done
}
#
__filterFunctions () {
    #
    __print_variables ctbankixFunctionList
    value="echoBoldWhite"
    [[ " ${ctbankixFunctionList[*]} " =~ " ${value} " ]] && echo "$value in list"
    value="echoBoldWhiteOnGreen"
    echo -e "second value: $value""\n"

    for ctbankixFunction in "${ctbankixFunctionList[@]}"
    do
        echo -e "$ctbankixFunction""\n"
        [[ "${value}" =~ "$ctbankixFunction" ]] && echo "$value in list"
    done
    echo -e "exclude the functions which names start with: "
    __print_variables functions2ExcludeFrom_List2
    return

    # [[ "${value}" =~ "${ctbankixFunctionList[*]}" ]] && echo "$value in list"

    # return
    # echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    # for WORD in cat mouse fly; do
    #     if [[ "$WORD" =~ ^(cat|dog|horse)$ ]]; then
    #         echo "$WORD is in the list"
    #     else
    #         echo "$WORD is not in the list"
    #     fi
    # done
}
#
listFunctions () {
    listAllFunctionNames
}

__removeFromArray () {
    # make use of a nameref 
    # https://stackoverflow.com/questions/10582763/how-to-return-an-array-in-bash-without-using-globals
    # make use of unset 'array[i]'
    # https://stackoverflow.com/questions/16860877/remove-an-element-from-a-bash-array
    local debug=1
    local -n array=$1
    [[ $debug == 0 ]] && echo -e "1 : ${#array[@]}""\n"
    local -n delete=$2
    [[ $debug == 0 ]] && echo -e "2 : ${#delete[@]}""\n"
    # echo "Hansi"
    #[[ $debug == 0 ]] && echo -e "1 : ${#array[@]}\n2 : ${#delete[@]}\n"
    for target in "${delete[@]}"; do
        [[ $debug == 0 ]] && echo "$target"
        for i in "${!array[@]}"; do
        if [[ ${array[i]} == $target* ]]; then
            [[ $debug == 0 ]] && echo "Treffer: array i :: ${array[i]} target:: $target"
            unset 'array[i]'
        fi
        done
    done
}
#
listUserFunctions () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    declare -a myfunctionList
    IFS=$'\n'
    for f in $(declare -F); do
        myfunctionList+=("${f:11}")
    done
    local debug=1
    [[ $debug == 0 ]] && echo -e "Anzahl: ${#myfunctionList[@]}"
    __removeFromArray myfunctionList ctbankixFunctionList
    [[ $debug == 0 ]] && echo "myfunctionList remaining 1 : ${#myfunctionList[@]}"
    __removeFromArray myfunctionList functions2ExcludeFrom_List2
    [[ $debug == 0 ]] && echo "myfunctionList remaining 2 : ${#myfunctionList[@]}"

    for function in "${myfunctionList[@]}"; do
        echo -e " - $function"
    done
}
#
listAllFunctionNames () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #declare -F
    IFS=$'\n'
    for f in $(declare -F); do
        output="${f:11}"
        echo " - $output"
    done
}
#

# 
iso_resources_mutability_check () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #echo -e "$(lsattr $WORK_DIR)""\n"
    echo -e "$(lsattr -RV $MAIN_RESOURCE_DIR)""\n"
}
__resource_mutability_check () {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #echo -e "$(lsattr $WORK_DIR)""\n"
    echo -e "$(lsattr -RV $1)""\n"
}
iso_resources_change_2_immutable () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "$(chattr +i -RV $MAIN_RESOURCE_DIR)""\n" 
    iso_resources_mutability_check
}
__resource_change_2_immutable () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "$(chattr +i -RV $1)""\n" 
    __resource_mutability_check $1
}

iso_resources_reset_2_mutable () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "$(chattr -i -RV $MAIN_RESOURCE_DIR)""\n"
    iso_resources_mutability_check
}
__reset_resource_2_mutable () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e "$(chattr -i -RV $1)""\n"
    __resource_mutability_check $1
}

#
# # 
# iso_resources_mutability_check () {
#     echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
#     #echo -e "$(lsattr $WORK_DIR)""\n"
#     echo -e "$MAIN_RESOURCE_DIR""\n"
#     #return
#     echo -e "$(lsattr -RV $MAIN_RESOURCE_DIR)""\n"
# }
# iso_resources_change_2_immutable () {
#     check4RootUser "${FUNCNAME[0]}"
#     echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
#     echo -e "$(chattr +i -RV $MAIN_RESOURCE_DIR)""\n" 
#     iso_resources_mutability_check
# }
# iso_resources_reset_2_mutable () {
#     check4RootUser "${FUNCNAME[0]}"
#     echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
#     echo -e "$(chattr -i -RV $MMAIN_RESOURCE_DIR iso_resources_mutability_check
# }
#
# listAllFunctionNames () {
#     echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
#     #declare -F
# MAIN_RESOURCE_DIRin $(declare -F); do
#         output="${f:11}"
#         echo -e "output: |$output|""\n"
#     #
#         for ctbankixFunction in "${ctbankixFunctionList[@]}"
#         do
#             echo -e "$ctbankixFunction""\n"
#             if [[ "${output}" =~ "$ctbankixFunction" ]]
#             then
#                 echo "$output in list"
#             else 
#                 echo "$output not in list"
#             fi
#         done
#         #echo " - $output"
#     done
# }
# #
# 
#
master-0 () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e " - - - - - - - - - - - - - - - "
    makeClean
    createInfrastructure
    isoCreate
    # unpackImmutableSourceIsoImage && \
    # #__checkUnpackedSourceIsoImage && \
    # # unpackSquashfs 
    # fs_in_work-unpackSquashfs
    # #&&  \

    # # fs_in_work-purgeUnwanted && \
    # # fs_in_work-upgradePackages && \
    # # fs_in_work-upgradeKernel
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}


unpackIsoAndCreate_fs_in_work () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    echo -e " - - - - - - - - - - - - - - - "
    unpackImmutableSourceIsoImage 
    fs_in_work-unpackSquashfs
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}
### >>>>>>>>>>>>>>>>>>>> personalize <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#

function fs_in_work-setup__kernelprefs {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local TARGET_FILE="$(getChrootEnv)""$(getVar ETC_APT_PREF_KERNEL_PATH)"
    echo -e "Zielverzeichnis: $TARGET_FILE""\n"
    #return
    cat > "$TARGET_FILE" << EOF
# wjp 2022-03-23
Package: linux-image-amd64  linux-headers-amd64
Pin: release o=debian*
Pin-Priority: -1 
# wjp end
EOF
}

function fs_in_work_chrooted-setup_sourceslist {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local TARGET="$(getChrootEnv)""$(getVar ETC_APT_SOURCES_LIST_PATH)"
    echo -e "Zielverzeichnis: $TARGET""\n"
    #return
    #cat > /tmp/wjp_sources.list << EOF
    cat > "$TARGET" << EOF
# wjp 2023-06-19 # version 0
deb http://ftp.halifax.rwth-aachen.de/debian/ bookworm main contrib non-free non-free-firmware
deb-src http://ftp.halifax.rwth-aachen.de/debian/  bookworm main contrib non-free non-free-firmware
#
# security-Konf -> entnommen: http://security.debian.org 2021-09-01
deb http://security.debian.org/debian-security bookworm-security main contrib non-free
deb-src http://security.debian.org/debian-security bookworm-security main contrib non-free
#
#
# bookworm-updates, previously known as 'volatile'
deb http://ftp.halifax.rwth-aachen.de/debian/ bookworm-updates main contrib non-free
deb-src http://ftp.halifax.rwth-aachen.de/debian/ bookworm-updates main contrib non-free
#
deb http://deb.debian.org/debian bookworm-backports main contrib non-free
# wjp end
EOF
}
#
function fs_in_work-setup__keyboard {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local TARGET_FILE="$(getChrootEnv)""$(getVar ETC_DEFAULT_KEYBOARD_PATH)"
    echo -e "Zieldatei: $TARGET_FILE\n"
    #return
    cat > "$TARGET_FILE" << EOF
# KEYBOARD CONFIGURATION FILE
# Consult the keyboard(5) manual page.

XKBMODEL="pc105"
XKBLAYOUT="de"
XKBVARIANT="nodeadkeys"
XKBOPTIONS=""
BACKSPACE="guess"

EOF
}

#
function fs_in_work-setup__wpa_supplicant {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local TARGET_FILE="$(getChrootEnv)""$(getVar ETC_WPA_SUPPLICANT_PATH)"
    echo -e "Zieldatei: $TARGET_FILE\n"
    #return
    cat > "$TARGET_FILE" << EOF
# wpa_supplicant.conf boesetante
# Consult https://www.cyberciti.biz/faq/intel-wifi-on-debian-linux-when-you-get-firmware-failed-to-load-iwlwifi-8265-36-error/
network={
        ssid="boesetante"
        psk=0493bca4c95a5ae6e848b8f88ea13a88f25a04a03d12ed91a9c09d319b10d143
}
ctrl_interface=/run/wpa_supplicant
update_config=1

EOF
}


function fs_in_work-setup__bashrc {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local TARGET_FILE="$(getChrootEnv)$(getVar ETC_SKEL_BASHRC_PATH)"
    echo -e "Zielverzeichnis: $TARGET_FILE\n"
    #return
    ## nur anhaengen
    cat >> "$TARGET_FILE" << EOF
setxkbmap de
EOF
}

function fs_in_work-setup__locale {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local TARGET_FILE_1="$(getChrootEnv)$(getVar ETC_LOCALE_PATH)"
    local TARGET_FILE_2="$(getChrootEnv)$(getVar ETC_DEFAULT_LOCALE_PATH)"
    echo -e "Zielverzeichnis 1: $TARGET_FILE_1\n"
    echo -e "Zielverzeichnis: $TARGET_FILE_2\n"
    #return
    cat > "$TARGET_FILE_1" << EOF
de_DE.UTF-8 UTF-8
EOF
    cat > "$TARGET_FILE_2" <<EOF
LANG=de_DE.UTF-8 
EOF
}
#

function setTsharkServiceMode {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #
    chrootPrepareAndMount
    local CHROOT_ENV="$(getChrootEnv)"
    chroot "$CHROOT_ENV"/ chmod 755 /usr/local/bin/tshark-capture
    chroot "$CHROOT_ENV"/ chmod 644 "$CHROOT_ENV"/lib/systemd/system/tshark-capture.service
    chroot "$CHROOT_ENV"/ ln -s /lib/systemd/system/tshark-capture.service /etc/systemd/system/multi-user.target.wants/tshark-capture.service 
    chrootRestoreAndUmount
}

function fs_in_work-setup__tsharkService {
    #
    #[ -L ${my_link} ] && [ -e ${my_link} ]
    # https://stackoverflow.com/questions/5767062/how-to-check-if-a-symlink-exists

    # # 4. Check if ~/bin/script is a symlink.
    # if [[ -L "$HOME/bin/script" && -e "$HOME/bin/script" ]]; then
    #   echo "It's a link!"
    # else
    #   echo "Might be a link, but it doesn't exist!"
    # fi
    # https://koenwoortman.com/bash-script-check-if-file-is-symlink/
    #
    # Achtung: das executable hat jetzt die Extension .sh!!!
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local CHROOT_ENV="$(getChrootEnv)"
    echo -e "Zielverzeichnis: $CHROOT_ENV\n"
    #
    # Attention: 
    # function uses a helper script which it writes into the chroot environment
    # the here document for execution in the chroot environment doesn't allow variables
    # Alternative:
    # use the chroot $CHROOTENV/ ..... syntax
    #
    local HELPER_SCRIPT_PATH="$CHROOT_ENV"/tmp/helper.sh
    echo -e "helper script at: $HELPER_SCRIPT_PATH"
    #
    local TSHARK_BASH_SCRIPT_PATH="$(getVar TSHARK_BASH_SCRIPT_PATH )"
    local FULL_TSHARK_BASH_SCRIPT_PATH="$(getChrootEnv)""$TSHARK_BASH_SCRIPT_PATH" #   /usr/local/bin/tshark-capture.sh
    echo -e "$FULL_TSHARK_BASH_SCRIPT_PATH""\n"
    #
    local TSHARK_SYSTEMD_SERVICE_CONFIG_PATH="$(getVar TSHARK_SYSTEMD_SERVICE_CONFIG_PATH)"
    local FULL_TSHARK_SYSTEMD_SERVICE_CONFIG_PATH="$(getChrootEnv)""$TSHARK_SYSTEMD_SERVICE_CONFIG_PATH" # /lib/systemd/system/tshark-capture.service
    echo -e "$FULL_TSHARK_SYSTEMD_SERVICE_CONFIG_PATH""\n"
    #
    local TSHARK_SYSTEMD_SYSTEM_MULTI_USER_TARGET_PATH="$(getVar TSHARK_SYSTEMD_SYSTEM_MULTI_USER_TARGET_PATH)"
    echo -e "$TSHARK_SYSTEMD_SERVICE_CONFIG_PATH links to $TSHARK_SYSTEMD_SYSTEM_MULTI_USER_TARGET_PATH""\n"
    #
    #
    cat > "$HELPER_SCRIPT_PATH" << EOF
#!/bin/bash
# start helper script
#ls && echo -e "\n"
echo -e "pwd: " $(pwd) "\n"
echo "'dollardollar' $$"
# neu
[ ! -d "/capture" ] && mkdir /capture
#mkdir /capture
chmod 755 $(getVar TSHARK_BASH_SCRIPT_PATH)
chmod 644 $(getVar TSHARK_SYSTEMD_SERVICE_CONFIG_PATH)
ln -s $(getVar TSHARK_SYSTEMD_SERVICE_CONFIG_PATH) $(getVar TSHARK_SYSTEMD_SYSTEM_MULTI_USER_TARGET_PATH) 
# end helper script
EOF
    #
    # create bash script to execute tshark
    #
    cat > "$FULL_TSHARK_BASH_SCRIPT_PATH" << EOF
#!/bin/bash
# wjp 2022-03-26
tshark -q -E separator=';' -T fields -e frame.time_epoch -e eth.src -e frame.len -b filesize:10000 -b files:100 -w /capture/tshark
#
EOF
    #
    # configure it into systemd 
    #
    cat > "$FULL_TSHARK_SYSTEMD_SERVICE_CONFIG_PATH" <<EOF
# $(getVar TSHARK_SYSTEMD_SERVICE_CONFIG_PATH) should equal
# /lib/systemd/system/tshark-capture.service

[Unit]
Description=Tshark Capture Service
After=multi-user.target

[Service]
Type=idle
ExecStart=$(getVar TSHARK_BASH_SCRIPT_PATH )

[Install]
WantedBy=multi-user.target

EOF
    #
    #
    # now integrate tshark into fs_in_work
    #
    chrootPrepareAndMount   
    #
    chroot "$CHROOT_ENV" /bin/bash << "EOT"
#ls && echo -e "\n"
echo -e "pwd: " $(pwd) "\n"
echo "'dollardollar' $$"
source /tmp/helper.sh    
exit

EOT

    chrootRestoreAndUmount 

    rm "$HELPER_SCRIPT_PATH" && echo -e "$(echoBoldWhiteOnBlue '$HELPER_SCRIPT_PATH removed')" 
    #
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}

fs_in_work-UpgradePackagesAndKernel () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #
    HOME_DIR="$(pwd)"
    BASE_DIR=$(getVar BASE_DIR)
    TARGET=$(getVar FS_IN_WORK_DIR)
    echo -e "$(echoBoldWhiteOnBlue  'personalising filesystem for live-iso')""\n"
    echo "Directory at start: $HOME_DIR"
    echo "    WORK BASE_DIR : $BASE_DIR"
    echo
    echo "${FUNCNAME[0]}: configure $TARGET"
    echo "====================================="
    echo
    date
    #
    if [ -d "$TARGET" ]
    then
	    echo "$TARGET found and going on..."
    else
	    echo "Error: $TARGET not found!"
	    return
    fi
# 
    fs_in_work-upgradePackages
    fs_in_work-upgradeKernel
    echo -e "HOME_DIR: $HOME_DIR" || exit
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}

isoRePack () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"

    ### WJP 2022-04-24 
    #fs_in_work-UpgradePackagesAndKernel

    ## createNewFileystemSquashfs
    fs_in_work-MakeNewSquashfs
    replaceFilesystemSquashfs
    isoPack
    echo -e "HOME_DIR: $HOME_DIR" || exit
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}

fs_in_work-SetupPersonalVersion () {
    check4RootUser "${FUNCNAME[0]}"
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    #
    HOME_DIR="$(pwd)"
    BASE_DIR=$(getVar BASE_DIR)
    TARGET=$(getVar FS_IN_WORK_DIR)
    echo -e "$(echoBoldWhiteOnBlue  'personalising filesystem for live-iso')""\n"
    echo "Directory at start: $HOME_DIR"
    echo "    WORK BASE_DIR : $BASE_DIR"
    echo
    echo "${FUNCNAME[0]}: configure $TARGET"
    echo "====================================="
    echo
    # read -r -p "start now? [y/N] " questionResponse
    # echo 
    # if [[ $questionResponse != [yJ] ]]
    # then
	#     exit
    # fi
    #
    #set -o xtrace
    #
    date
    #
    if [ -d "$TARGET" ]
    then
	    echo "$TARGET found and going on..."
    else
	    echo "Error: $TARGET not found!"
	    return
    fi
# 
    fs_in_work-purgeUnwanted 
    fs_in_work-upgradePackages
    fs_in_work-installNewPackages
#
    fs_in_work-setup__keyboard && echo -e "$(echoBoldWhiteOnGreen 'keyboard configured')""\n"
    fs_in_work-setup__bashrc && echo -e "$(echoBoldWhiteOnGreen 'xdkmap set for de')""\n"
    fs_in_work-setup__locale && echo -e "$(echoBoldWhiteOnGreen 'locale reconfigured')""\n"
    fs_in_work-setup__skeleton && echo -e "$(echoBoldWhiteOnGreen 'etc/skel copied')""\n"
    fs_in_work-setup__tsharkService && echo -e "$(echoBoldWhiteOnGreen 'tshark service installed')""\n"
    fs_in_work-setup__connmanMainConf && echo -e "$(echoBoldWhiteOnGreen 'connman online check disabled')""\n"
#
    createLatestSplash
    fs_in_work-manage-plymouth
    #
    echo -e "HOME_DIR: $HOME_DIR" || exit
    echo -e "\nFunktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)\n"
}
#
fs_in_work-setup__connmanMainConf () {

    local targetPath="$(getChrootEnv)$(getVar ETC_CONNMAN_CONF_PATH)"
    echo -e "$targetPath""\n" 

    sed -i "/# EnableOnlineCheck/s/^# //g" $targetPath


}


testArrayIndex () {
    declare -A myArray
    myArray=([red]=1 [orange]=2 [green]=3)
    echo ${myArray['orange']}
    #prompt="select function ( use  to leave) : "


}


function intro {
    echo
    echo "support tool ( $(basename $0) )for building live-iso on debian 11 "
    echo "====================================="
    echo


}

function becomeSudo {
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    if [ -z "$SUDO_COMMAND" ]
    then
        local executable="bash $(getVar HOME_DIR)/$(basename $0)"
        #echo "$executable"
        mntusr=$(id -u) grpusr=$(id -g) sudo bash $0 
        #exit 0
        return
    fi

### https://tldp.org/LDP/abs/html/recursionsct.html
# if [ -z "$SUDO_COMMAND" ]
# then
#    mntusr=$(id -u) grpusr=$(id -g) sudo $0 $*
#    exit 0
# fi

# # We will only get here if we are being run by sudo.
# /bin/mount $* -o uid=$mntusr,gid=$grpusr

# exit 0


# We will only get here if we are being run by sudo.
#/bin/mount $* -o uid=$mntusr,gid=$grpusr
    echo -e "nach if"
    exit 0
}

fs_in_work-setup__skeleton () {
    check4RootUser
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local SKEL_DIR="$MAIN_RESOURCE_DIR"/SKELETON/skel
    #local TARGET_DIR=$(getChrootEnv)$(getVar ETC_SKEL_TARGET_DIR)
    local TARGET_DIR=$(getChrootEnv)/etc/
    echo -e "source: $SKEL_DIR""\n"
    echo -e "target: $TARGET_DIR""\n"
    #return
    if [ ! -d "$SKEL_DIR" ]
    then 
        echo "$SKEL_DIR does not exit"
    else 
        /usr/bin/cp -r -p $SKEL_DIR $TARGET_DIR
        result=$?
        echo "skel copy result: $result" 
        #setVar_Dir SKELETON $SKEL_DIR
        #setVar_Dir XX "Zara"
        #printVars  #&& return
    fi
    echo -e "\n\n""Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]}) $(echoBoldWhiteOnGreen Fertig)""\n" 
}
# #
# test_timeString() {
#     echo -e "The number of positional parameter : $#\n"
#     echo -e "$1\n"
#     local timeString=$(/usr/bin/date +"%y-%m-%d_%H_%M"| cut -c1-15)
#     echo -e "timeString: $timeString"
# }
#
createIsoVersionString () {
    local -n isoVersionString="$1"
    local timeString=$(/usr/bin/date +"%y-%m-%d_%H_%M"| cut -c1-15)
    #isoVersionString="$timeString-kernel-$(getVar KERNEL_VERSION)-live-$(getVar DEBIAN_LIVE_VERSION)"
    local version_1=$(getVar DEBIAN_LIVE_VERSION)
    #echo $version_1
    local debian_live_version=${version_1//./-}
    #echo $debian_live_version
    isoVersionString="$(getVar KERNEL_VERSION)-live-$debian_live_version-$timeString"
    #echo -e "$isoVersionString""\n"
    #new=$(echo $isoVersionString | wc )
    #echo -e "$new"
}

__createSplash() {
        # echo -e "Debian Live Version: ${var['DEBIAN_LIVE_VERSION']}"
        # echo -e "Kernel Version: ${var['KERNEL_VERSION']}"
        # echo -e "\---"
        # echo -e "versionPainter: $(getVar ISOLINUX_SPLASH_VERSIONPAINTER)"
        # echo -e "base png:  $(getVar ISOLINUX_SPLASH_BASE_PNG)"
    #
    cmd="$(getVar ISOLINUX_SPLASH_VERSIONPAINTER) -t \"Debian ${var['DEBIAN_LIVE_VERSION']} Kernel ${var['KERNEL_VERSION']}\" -r $(getVar ISOLINUX_SPLASH_BASE_PNG) -o $1/splash.${var['KERNEL_VERSION']}.png"
    local name_of_the_new_splashfile
    name_of_the_new_splashfile= eval $cmd
    echo ${name_of_the_new_splashfile}
}

createLatestSplash () {
    check4RootUser
    echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local splash_create_dir=/tmp
    local splash_target_dir=${var['ISO_ISOLINUX_DIR']}
    #echo -e "splash_resource_dir:  $splash_resource_dir\n----"
    echo -e "  splash_target_dir:  $splash_target_dir\n----"
    #echo -e "the variable : ${var['ISO_ISOLINUX_DIR']}"
    #return
    ## echo -e " ... bin im $(pwd)"
    # open the splashes directory
    #__reset_resource_2_mutable $splash_dir
    # create the new splash file
    new_splash_file=$(__createSplash $splash_create_dir)
    # ok, it should be there
    echo -e "the new splash_file is: $new_splash_file"
    #echo -e "now set soft link: $splash_dir/splash.png"
    ## (cd $splash_dir && ln -sf $new_splash_file splash.png && rm -f splash800x600.png && convert-im6.q16 $new_splash_file -scale 800x600 splash800x600.png)
    (cd $splash_target_dir && 
        echo -e "subshell working in: $(pwd)" &&
        rm -f splash.png && 
        convert-im6.q16 $new_splash_file -scale 640x480 splash.png && 
        rm -f splash800x600.png && 
        convert-im6.q16 $new_splash_file -scale 800x600 splash800x600.png &&
        cd $(getVar UEFI_GRUB_DIR) &&
        echo -e "subshell working in: $(pwd)" && 
        rm -f splash.png && 
        convert-im6.q16 $new_splash_file -scale 800x600 splash.png &&
        echo -e "all png in place"
        )

    ## ls -ltra $splash_dir
    # now close the splashes directory
    #__resource_change_2_immutable $splash_dir
    ## echo -e " ... bin im $(pwd)"
    #cp $splash_dir/splash.png /tmp/
    return
}

createLinkLatestIso () {
    #echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
    local iso_dir=$(getVar TARGET_DIR_4_NEW_ISO)
    echo -e "iso_dir: $iso_dir"
    #return
    local iso_dir_link_latest_iso=$iso_dir/latest.iso
    # for entry in "$search_dir"/*
    # do
    #     echo "$entry"
    #     echo -e "\n""by readlink""\n"$(readlink -e "$entry")"\n\n"
    # done
    # #ln -s "$(readlink -e "$2")" "$1/link"
    echo -e "\n----"
    latestFile=$(find $iso_dir -type f -exec ls -1t "{}" + |head -n 1)
    echo -e "latest: $latestFile\n----"
    #return
    #ln -s "$(readlink -e "$latestFile")" "$search_dir/latest.iso"
    [ -f "$iso_dir_link_latest_iso" ] && echo "$iso_dir_link_latest_iso exists - removing it!" && rm "$iso_dir_link_latest_iso"
    ln -s "$latestFile" "$iso_dir_link_latest_iso"
    # -1 single column
    #find $search_dir -printf "%T@ %Tc %p\n" | sort -n
    #find $search_dir -type f -printf "\n%p" 
    #find $search_dir -type f -printf "\n%AT %p" | head -n 2
    #ls -ltc $search_dir | head -n 1
    echo -e "latest: $latestFile\n----"
}
#
__createLinkInConfig4RunIso() {
    # echo -e "Argument: $1"
    # echo -e "config file is: $CONFIG_FILE"
    # 1.) remove the line with starts with X_LATEST_ISO
    #sed -i '/X_LATEST_ISO/d' $CONFIG_FILE
    # 1.) out comment 
    sed -i 's/^X_LATEST/# &/' $CONFIG_FILE
    # 2.) add a new line to the $CONFIG_FILE
    echo "X_LATEST_ISO=$1" >> $CONFIG_FILE
}
# createLinkInConfig4RunIso () {
#     echo -e "Funktion: $(echoBoldWhiteOnMagenta  ${FUNCNAME[0]})\n"
#     #
#     local iso="bbb.iso"
#     __createLinkInConfig4RunIso $iso
# }
# <<<<<<<<<< Konstanten und Funktionen <<<<<<<<<<
# here we start
# Abbruch des Skripts bei Fehlern
set -e
#
# ctbankix: Debug-Ausgaben fuer den restlichen Teil des Skripts aktivieren
export PS4='$(__CheckExitCodePreviousCommand)\n\n$(echoDateTime) $(echoBoldWhiteOnMagenta "LN: ${LINENO}") $(setTextBold)$(setTextWhite)$(setBackgroundCyan)${BASH_COMMAND}$(resetText)\n'
setVariables_All
intro
#echo "Aufrufparameter kontrollieren"
echo "           $(basename $0), called with $# parameter(s)"
if [ $# == 0 ]; then
    __dialog
elif [ $# \> 0 ]; then
    # # \> , not > !!!
    # if [ $# == 1 ]
    # then
    #     echo -e "  calling: $1"
    # else 
    #     echo -e "  calling: $@"
    # fi
    echo -e "  calling: $@""\n"
    $@
fi

# alt
# echo "           $(basename $0), called with $# parameter(s)"
# if [ $# == 1 ]; then
#     echo -e "  calling: $1\n"
#     $"$1"
#     exit
# elif [ $# == 2 ]; then
#     echo -e "  calling: $1 with parameter $2"
#     $@
#     exit
# fi
# # else: present dialog
# __dialog
# >>>>>>>>>> work - now ! >>>>>>>>>>
