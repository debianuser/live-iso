# live-iso

### privately used repository

It is open for source code readers and courageous people willing to try out something new ...

## this is version 2024-02-26.2 of the isomaster.sh 

### create a read-only iso image 

... to make the connection of my personal computer to a foreign server as private and as secure as possible. Main focus is on firefox and web connections.

### ... work in progress

Dear guest, please be aware that code and comments are subject to change without further notice. 

### update to debian 12 - so far not documented

  - introduces config file


### remaster debian 11 live iso

This is about remastering the debian live iso with a vanilla ( aka
pristine ) kernel, which is patched to ignore hard disks and nvme
devices. This kernel ( rsp. the installation files in \*.deb format) has
to be provided.

My sources are:
  - <https://www.kernel.org/>
  - <https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/>

For technical aspects see also:
  - <https://wiki.debian.org/RepackBootableISO>

#### isomaster.sh: usecase remastering 

  - sudo bash isomaster.sh menu4Remaster

``` 

     Kernel is version 5.17.5
Debian live is version 11.3.0

support tool ( isomaster.sh )for building live-iso on debian 11 
=====================================

           isomaster.sh, called with 1 parameter(s)
  calling: menu4Remaster

calling __menuImpl
Funktion: menu4Remaster

 1) makeClean
 2) createInfrastructure
 3) unpackIsoAndCreate_fs_in_work
 4) fs_in_work-SetupPersonalVersion
 5) -
 6) fs_in_work-upgradePackages
 7) fs_in_work-upgradeKernel
 8) -
 9) fs_in_work-MakeNewSquashfs
10) replaceFilesystemSquashfs
11) -
12) isoPack
13) runNewIso
14) return
select function ( use ^C or  14 to leave, <enter> to re-list options of menu4Remaster} ) : 13
Funktion: runNewIso

activeNetwork x

no active network
Netzwerk default gestartet

Netzwerk default gelöscht

done

select function ( use ^C or  14 to leave, <enter> to re-list options of menu4Remaster} ) : ^C


``` 




#### be aware of "de" / "german" setup for the locale / keymap ... \!

Although the documention is written in english the tool sets up the
locale, the keymap (and so forth) with "de" rsp. "german" exclusively\!
For other languages the tool has to be adapted.

## requirements

The following essentials have to be available:

  - a workdir (variable: **WORK\_DIR**)
  - a resources dir (variable: **MAIN\_RESOURCE\_DIR**)

These directories **should belong to \<user\>** - not root. (We will
work with root some times later but for developping and manual additions
or corrections it is more convenient with owner \<user\>)

The resources dir **must contain**:

  - a debian-live-iso in *Debian11-ISO*
  - debian kernel installation files in *Kernels/latest*

The resources dir **may contain**

  - *SKELETON* dir with skel for /etc/skel
  - *Splashes* dir with a splash.png to characterize the edition which
    is build

The tool checks the presence of the must conditions when starting. If
the paths are different, open the script and edit these lines:

    ## >>>>> live-iso >>>>>>>>>>>>>>>
    # essentials
    MAIN_RESOURCE_DIR=/Resources4LiveIso
    #
    WORK_DIR=/live-iso
    [ ! -d "$WORK_DIR" ] && echo "$WORK_DIR is missing - check!" && exit
    DEBIAN_ISO="$MAIN_RESOURCE_DIR"/Debian11-ISO/debian-live-11.3.0-amd64-lxde.iso
    [ ! -f "$DEBIAN_ISO" ] && echo "$DEBIAN_ISO is missing  - check!" && exit
    # latest kernel version has to be linked via 'ln -s' on softlink "latest"
    KERNEL_PACKAGE_DIR="$MAIN_RESOURCE_DIR"/Kernels/latest
    [ ! -d "$KERNEL_PACKAGE_DIR" ] && echo "$KERNEL_PACKAGE_DIR is missing  - check!" && exit
    #

### rescue function 'chrootRestoreAndUmount' - just keep this in mind

The chroot command is used several times in the tool. Errors in the
script code may cause the script to abort and leave the environment in
an unwanted and troublesome state with mount points still active. This
can cause trouble (i.e. Visual Studio Code often crashed when this situation
occured during testing). Try to call the function

  - clear && bash isomaster.sh **chrootMountCheck**

to view the situation and

  - clear && sudo bash isomaster.sh **chrootRestoreAndUmount**

to unmount the persisting mount points in the chroot environment.

### how to view variables and functions

  - clear && bash isomaster.sh printMyVars

to view the variables in use

  - clear && bash isomaster.sh listFunctions

to find all functions provided and

  - clear && bash isomaster.sh listFunctions2

for a somewhat reduced function list :-)

## essentials - starting point

A debian-live iso file and kernel files have to be provided. This is the
location of my choice: "/Resources4LiveIso". This location is hard wired
in the tool

  - MAIN\_RESOURCE\_DIR=/Resources4LiveIso

Change this if your directory differs.

Overview my /Resources4LiveIso (2022-04-08)

``` 
debianuser@orion-7:/Resources4LiveIso$ tree -L 3
.
├── Debian11-ISO
│   ├── debian-live-11.2.0-amd64-lxde.iso
│   ├── debian-live-11.3.0-amd64-lxde.iso
│   ├── SHA512SUMS
│   └── SHA512SUMS.sign
├── Kernels
│   ├── 5-16-17
│   │   ├── linux-headers-5.16.17_5.16.17-1_amd64.deb
│   │   ├── linux-image-5.16.17_5.16.17-1_amd64.deb
│   │   └── linux-libc-dev_5.16.17-1_amd64.deb
│   ├── 5-17-1
│   │   ├── linux-headers-5.17.1_5.17.1-1_amd64.deb
│   │   ├── linux-image-5.17.1_5.17.1-1_amd64.deb
│   │   └── linux-libc-dev_5.17.1-1_amd64.deb
│   └── latest -> 5-17-1
├── SKELETON
│   └── skel
│       ├── Desktop
│       └── trez_goarem.2019.jpg
└── Splashes
    └── 11.3.0
        └── splash.png

10 directories, 12 files

```

While Debian11-ISO and Kernels are mandatory directories, SKELETON and
Splashes are voluntary. 

Be careful - the **kernel version of choice** has to
be **softlinked** to **"latest"**! Function **retrieveVersionFromLinuxKernelVanillaPackages** evaluates the real
path of latest and further on this is the kernel version used in the
script.

### working directory

My working directory is **/live-iso**

It has to be created by hand:

  - create working directory /live-iso
  - change owner of /live-iso to your user


  - **/live-iso and it's descending directories belong to the user who
    is going to build live-iso**

(That makes live for developping with the tool a lot easier :-)

Based on the directories above, function **createInfrastructure**
produces the other needed directories:

    creating /live-iso/11.2.0
    creating /live-iso/11.2.0/Backup
    creating /live-iso/11.2.0/ISO_MOUNT_POINT
    creating /live-iso/Resources/11.2.0/mbr
    creating /live-iso/11.2.0/Targets

The tool sets the owner of the subdirectory to the owner of /live-iso when creating these
    directories.

#### function makeClean

At the time I write this the working directory is /live-iso/11.2.0 and
this reflects the debian version I'm working with. By calling the makeClean function the root user can remove the
working directory below /live-iso - in my case it will remove the
"11.2.0" subdirectory of /live-iso/11.2.0.


### Resources

Just for information: function **createMbrTemplate** creates
/live-iso/11.2.0/MBR/isohdpfx.bin, the master boot record to
boot the iso later both in uefi and bios mode.

##### Immutability (use by choice)

It is a good idea to protect the Resources subdirectory against
accidental destruction - use function
**iso\_resources\_change\_2\_immutable** as root. See the status with
**iso\_resources\_state\_check** and reset with
**iso\_resources\_reset\_2\_mutable**


### configuration codes in the script

#### kernel.pref

``` 
# 2022-02-16
Package: linux-image-amd64  linux-headers-amd64
Pin: release o=debian*
Pin-Priority: -1 
# end

```

#### sources.list

``` 
# 2021-08-30 # version 0
deb http://ftp.halifax.rwth-aachen.de/debian/ bullseye main contrib non-free
deb-src http://ftp.halifax.rwth-aachen.de/debian/ bullseye main contrib non-free
#
# security-Konf -> entnommen: http://security.debian.org 2021-09-01
deb http://security.debian.org/debian-security bullseye-security main contrib non-free
deb-src http://security.debian.org/debian-security bullseye-security main contrib non-free
#
#
# bullseye-updates, previously known as 'volatile'
deb http://ftp.halifax.rwth-aachen.de/debian/ bullseye-updates main contrib non-free
deb-src http://ftp.halifax.rwth-aachen.de/debian/ bullseye-updates main contrib non-free
#
deb http://deb.debian.org/debian bullseye-backports main contrib non-free
# end

```

