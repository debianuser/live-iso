### Follow convention X_<variable_name>
### ... makes debugging easy
# shellcheck shell=bash
# shellcheck disable=SC2034  # Unused variables left for readability
## 
# manually configured
# packages
X_myPackages=(\
       tzdata 
       task-german 
       squashfs-tools 
       cups 
       wswiss 
       wngerman 
       wogerman 
       aspell-de \
       hunspell-de-de 
       vim 
       dnsutils 
       mlocate 
       mc 
       colordiff  \
       apt-file 
       psmisc 
       tree \
       firmware-iwlwifi 
       wpasupplicant 
       iw 
       rfkill 
       wireless-tools \
       firmware-amd-graphics 
       firmware-realtek 
       firmware-linux \
       intel-microcode 
       iucode-tool 
       thermald \
       ausweisapp2 
       libifd-cyberjack6
)

#
X_SUBDIR_ISO=Debian12-ISO/12_5
#
X_FILE_ISO=debian-live-12.5.0-amd64-lxde.iso
###
### automatically configured
###
# X_LATEST_ISO=/live-iso/12.5.0/Targets/6.7.5-live-12-5-0-24-02-18_15_17.iso
# X_LATEST_ISO=/live-iso/12.5.0/Targets/6.7.6-live-12-5-0-24-02-26_11_35.iso
X_LATEST_ISO=/live-iso/12.5.0/Targets/6.7.6-live-12-5-0-24-02-26_16_58.iso
