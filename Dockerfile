FROM debian:latest

RUN apt-get update && apt-get install -y \
	rustc \
	rust-src \
	bindgen \
	clang \
	llvm \
	lld \
        bc \
        bison \
        build-essential \
        cpio \
        flex \
        libelf-dev \
        libncurses-dev \
        libssl-dev \
        dwarves \
        python3 \
        rsync \
        kmod \
	lsb-release \
	debhelper \
        vim-tiny
