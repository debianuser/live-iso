## see Dockerfile

   - podman build . -t compiler-6

## this is old documentation - maybe helpful

# live-iso kernel build - preparation steps

my objective: 

    - linux on an usb stick without access to  
    - hard disks (1) nor nvme devices (2)

additional target:

    - keeping it slender - no debug symbols (3)

criterion / test case:

    - /usr/bin/lsblk does not detect hard disk nor nvme device 

## steps:

    - get kernel files
    - verify them
    - unpack
    - patch libata.h
    - work on .config
    - compile 


## get kernel files

source: <https://www.kernel.org/>

Download tarball and pgp.

"KernelSource" is my working directory. I create a subdirectory similar to the notation of the kernel I'm going to download. 

    - example: 5-17-2 (hyphens)


debianuser@kappa-1:~/KernelSource$ tree -a -L 2

``` 
.
├── 5-16-17
│   ├── libata.h.orig
│   ├── linux-5.16.17-patched
│   └── linux-5.16.17.tar
├── 5-17-1
│   ├── libata.h.orig
│   ├── linux-5.17.1-patched
│   └── linux-5.17.1.tar
├── 5-17-2
│   ├── libata.h.orig
│   ├── linux-5.17.2-patched
│   ├── linux-5.17.2.tar
├── Patcher
│   └── patch.sh
├── XZ
│   ├── linux-5.16.17.tar.xz
│   ├── linux-5.17.1.tar.xz
│   └── linux-5.17.2.tar.xz
├── linux-5.17.2.tar
└── linux-5.17.2.tar.sign

11 directories, 22 files

``` 
## verify kernel authenticity

####  prepare gpg 

based on <https://www.kernel.org/signature.html>

    gpg --locate-keys torvalds@kernel.org gregkh@kernel.org

  - get keys 
  - *gpg --edit-key* \<.....\> 
  - ... and now *sign* =\> put trust in keys

#### unpack and verify

Kernel file in xz-format and *.tar.sign are present. 
Use unxz to unpack the kernel file.
Now verify the kernel tar package.

    unxz linux-5.17.2.tar.xz 
    gpg --verify linux-5.17.2.tar.sign linux-5.17.2.tar



The result should look like this:

``` 
gpg: Signature made Wed Apr 13 19:28:44 2022 CEST
gpg:                using RSA key 647F28654894E3BD457199BE38DBBDC86092693E
gpg: Good signature from "Greg Kroah-Hartman <gregkh@linuxfoundation.org>" [full]
gpg:                 aka "Greg Kroah-Hartman <gregkh@kernel.org>" [full]
gpg:                 aka "Greg Kroah-Hartman (Linux kernel stable release signing key) <greg@kroah.com>" [full]
```


## objective (1):  patch libata => no sata disks

This step has to be performed for each new kernel source:

    - ./Patcher/patch.sh 5.17.2

Carefull: This time it is "." (points).

./Patcher/patch.sh
``` 
    #!/bin/bash
    echo "Argument: $1"
    version="$1"
    dir=$(sed 's/\./-/g' <<<"$version")
    echo -e "version: $version\n"
    echo -e "    dir: $dir\n"
    #
    cp /home/debianuser/KernelSource/"$dir"/linux-"$version"/include/linux/libata.h /home/debianuser/KernelSource/"$dir"/libata.h.orig
    #
    sed -i '/^static inline unsigned int ata_dev_enabled(const struct ata_device \*dev)/{N;N;N;s/return ata_class_enabled(dev->class);/return dev->class == ATA_DEV_ATAPI;/}' /home/debianuser/KernelSource/"$dir"/linux-"$version"/include/linux/libata.h
    # 
    #
    mv ~/KernelSource/"$dir"/linux-"$version" /home/debianuser/KernelSource/"$dir"/linux-"$version"-patched
``` 

## use menuconfig for objective (2) and (3)

Once there is a .config file (i.e. from former compilations) you copy it into the "linux-n.nn.nnn-patched" directory - further down will be more instructions how to deal with it. 

In case there is no .config use "make menuconfig" to configure base settings for nvme support and disabling debug symbols.

### objective (2):  disable NVME support

``` 
make menuconfig 
```

    Linux Kernel Configuration
      └─> Device Drivers
         └─> NVME Support
            └─> ... go in and deactivate all

### objective (3): no debug info


``` 
make menuconfig 
```
    Linux Kernel Configuration
      └─> Kernel hacking
         └─> Compile-time checks and compiler options
            └─> Compile the kernel with debug info



## .config in the new kernel subdirectory

For completeness:

an "old" .config is the base for creating a "new" .config by using

``` 
yes "" | make oldconfig

### or better

make olddefconfig

```

This last step is done before starting compiling and it is documented there. 

see also:

<https://serverfault.com/questions/116299/automatically-answer-defaults-when-doing-make-oldconfig-on-a-kernel-tree>

<https://cs4118.github.io/dev-guides/debian-kernel-compilation.html>

Read more later in [.config](#.config) below.

## next step: compilation

## compiler-1

I build the vanilla/pristine kernel ( loaded from kernel.org ) with the help of a docker container.

For this purpose I created 

    /home/debianuser/KernelBuild/Compiler-1/BUILD

and copied my patched kernel sources into this directory.


#### some docker

quoted from: <https://phoenixnap.com/kb/docker-image-vs-container>

A Docker image is an immutable (unchangeable) file that contains the source code, libraries, dependencies, tools, and other files needed for an application to run.

Due to their read-only quality, these images are sometimes referred to as snapshots. They represent an application and its virtual environment at a specific point in time. This consistency is one of the great features of Docker. It allows developers to test and experiment software in stable, uniform conditions.

Since images are, in a way, just templates, you cannot start or run them. What you can do is use that template as a base to build a container. A container is, ultimately, just a running image. Once you create a container, it adds a writable layer on top of the immutable image, meaning you can now modify it.

see also (german): <https://www.ionos.de/digitalguide/server/knowhow/docker-image/>

#### lazydocker

As a helper for the work with docker and to keep order with images and containers I recommend lazydocker.

<https://github.com/jesseduffield/lazydocker>

#### Dockerfile for compiler-1

The image is build with a Dockerfile (big D at the beginning of the file name)

    FROM debian:latest
    
    RUN apt-get update && apt-get install -y \
            bc \
            bison \
            build-essential \
            cpio \
            flex \
            libelf-dev \
            libncurses-dev \
            libssl-dev \
            dwarves \
            python3 \
            rsync \
            kmod \
            vim-tiny

build image with 

  - **docker build . -t compiler-1**

(compiler-1 small letter!)

create container and run it:

  - **docker run -ti -v /home/debianuser/KernelBuild/Compiler-1/BUILD/:/work compiler-1**

The local BUILD directory of the host is now mounted as the directory /work within the container.

  - before: copy the patched kernel directory into the BUILD directory
  - in the container: change to this directory 

### <a name=".config"></a> .config

be aware: 

the .config 

    - I copied the .config of a former compilation into my linux-n.nn.nnn-patched directory.
    - As root in the container the following has to be done: 

``` 
    1  cd work/
    2  ls
    3  cd linux-5.17.2-patched/
    4  make olddefconfig
    5  make -j $(nproc) deb-pkg
```

On my machine with 6 processors (see model below) it takes about 20 minutes compile time. 

    model name      : Intel(R) Core(TM) i5-9600K CPU @ 3.70GHz


Afterwards the fresh *.deb packages are in the BUILD directory of the host.
